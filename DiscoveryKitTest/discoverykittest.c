/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "discoverykittest.h"
#include "hal.h"

#ifdef BOARD_ST_STM32VL_DISCOVERY
void discoveryKitTestMain(void){
	bool fast = false;
	for(;;){
		palTogglePad(GPIOC, GPIOC_LED3);
		chThdSleepMilliseconds(fast ? 10 : 100);
		palTogglePad(GPIOC, GPIOC_LED4);
		chThdSleepMilliseconds(fast ? 10 : 100);

		if(palReadPad(GPIOA, GPIOA_BUTTON)){
			chThdSleepMilliseconds(500);
			fast = !fast;
		}
	}
}

#endif

#ifdef BOARD_ST_STM32F4_DISCOVERY
void discoveryKitTestMain(void){
	bool fast = false;
	for(;;){
		palTogglePad(GPIOD, GPIOD_LED3);
		chThdSleepMilliseconds(fast ? 10 : 100);
		palTogglePad(GPIOD, GPIOD_LED4);
		chThdSleepMilliseconds(fast ? 10 : 100);
		palTogglePad(GPIOD, GPIOD_LED5);
		chThdSleepMilliseconds(fast ? 10 : 100);
		palTogglePad(GPIOD, GPIOD_LED6);
		chThdSleepMilliseconds(fast ? 10 : 100);

		if(palReadPad(GPIOA, GPIOA_BUTTON)){
			chThdSleepMilliseconds(500);
			fast = !fast;
		}
	}
}

#endif
