/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MODULECONF_H_
#define MODULECONF_H_

#include "shields.h"

/// @name Shield configuration
/// @{

#define SHIELD_TOP_ID SHIELD_ID_TEST
#define SHIELD_BOTTOM_ID SHIELD_ID_TEST

#define SHIELD_TEST_TOP_INIT_LEDS 1
#define SHIELD_TEST_BOTTOM_INIT_LEDS 1

/// @}

/// Feature configuration
/// @{

#define COMM_ENABLE_CAN TRUE
#define COMM_HEARTBEAT 1000

/// @}

#endif /* MODULECONF_H_ */
