/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#if !defined(TEST_TOKEN) || defined(__DOXYGEN__)
/// Test token.
/// Override it to non-zero to enable test token.
#define TEST_TOKEN 0
#endif

#include "module.h"
#include "communication.h"
#include "commserial.h"
#include "commtext.h"
#include "communicationtest.h"

#ifdef BOARD_ARMBOARD
#include "testshieldtest.h"
#else
#include "discoverykittest.h"
#endif

void moduleInit(void){
	testInit();
}
void moduleMain(void){
#ifdef BOARD_ARMBOARD
	testShieldTestMain();
#else
	discoveryKitTestMain();
#endif
}

bool_t commNmtStateChanging(uint16_t o, uint16_t n){
	(void)o;
	(void)n;
	return true;
}

void moduleSerialCommand(const CommSerial* serial){
	BaseSequentialStream* ss = (BaseSequentialStream*)serial->channel;
	if(serial->line->data[0] == 'l'){
		switch(serial->line->data[1]){
		case 't':
			prints(ss, "aTST");
			printx2(ss, COMM_MODULE_ID);
			println(ss);
			break;
#if TEST_TOKEN
		case 'T':
			prints(ss, "a");
			printx8(ss, TEST_TOKEN);
			println(ss);
			break;
#endif
		}
	}
	else if(serial->line->data[0] == 'b'){
		prints(ss, "baaaldddddddddddddddd\r");
	}
}

