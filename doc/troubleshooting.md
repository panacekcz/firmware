# Troubleshooting

## USB serial

### After connection, opening serial port reports "Device or resource busy", the first command is unrecognized

ModemManager may be trying to use the device. If you don't need it, uninstalling modemmanager solves the problem.

## CAN

### Module does not respond to CAN frames

The output mode must be configured to include the interfaces where CAN frames should be sent.
This can be don either by `o` serial command or by a PDO.
If you have a CAN bus connected by USB, put `slcan_init = "omcu\rt501404000000\r"`
to your `local.cfg`.