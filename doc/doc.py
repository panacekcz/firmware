#     Copyright (C) 2015  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.

import sys
import markdown
from markdown.extensions.headerid import HeaderIdExtension, slugify
from markdown.postprocessors import Postprocessor
from markdown.extensions import Extension
from markdown.util import etree

class HTMLFramePostprocessor(Postprocessor):
    def run(self, text):
        return '''<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style type="text/css">
            body{
                font: 10pt sans-serif;
            }
            table, td{
                border: 1px solid silver;
                border-collapse: collapse;
            }
            code{
                background-color: #eee;
            }
            em{
                background-color: #ffc;
            }
        </style>
    </head>
    <body>''' + text + '''
    </body>
</html>'''

class HTMLFrameExtension(Extension):
    def extendMarkdown(self, md, md_globals):
        md.postprocessors.add('frame', HTMLFramePostprocessor(md), '_end')

def headerId(value, separator):
    return 'markdown-header-'+slugify(value, separator)

header_extension = HeaderIdExtension(slugify=headerId)
extensions=['markdown.extensions.tables', header_extension, HTMLFrameExtension()]
markdown.markdownFromFile(input=sys.argv[1], output=sys.argv[2], safe_mode='replace', extensions=extensions)