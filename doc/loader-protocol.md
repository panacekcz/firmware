# Loader protocol

 Loader communicates with the host using ASCII text commands.
 Each command from the host is a line of at most 255 characters terminated by the `'\r'` or `'\n'` character.
 Empty lines are ignored, so you can use both unix and windows line endings.
 The loader responds by zero or more lines terminated by `'\r'`, depending on the command.
 By default the loader does not communicate on its own, it only reacts to the commands sent by the host.