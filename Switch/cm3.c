/*
    ChibiOS/RT - Copyright (C) 2006,2007,2008,2009,2010,
                 2011,2012,2013 Giovanni Di Sirio.
    Robot Firmware - Modified 2014 by Vlastimil Dort

    This file is part of ChibiOS/RT.

    ChibiOS/RT is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    ChibiOS/RT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

typedef unsigned uint32_t;
typedef unsigned char uint8_t;

#include "switch.h"
#include "mcu.h"
#include "io.h"
#include "base.h"
#include "functions.h"

typedef void  (*irq_vector_t)(void);
extern volatile uint32_t __main_stack_end__[];

__attribute__((naked, noreturn))
void switch_entry_point(void){
	// Disable interrupts
	__asm__ volatile("cpsid i");
	switch_procedure();
}

typedef struct {
  volatile uint32_t      *init_stack;
  irq_vector_t  reset_vector;
  irq_vector_t  nmi_vector;
  irq_vector_t  hardfault_vector;
  irq_vector_t  memmanage_vector;
  irq_vector_t  busfault_vector;
  irq_vector_t  usagefault_vector;
  irq_vector_t  vector1c;
  irq_vector_t  vector20;
  irq_vector_t  vector24;
  irq_vector_t  vector28;
  irq_vector_t  svcall_vector;
  irq_vector_t  debugmonitor_vector;
  irq_vector_t  vector34;
  irq_vector_t  pendsv_vector;
  irq_vector_t  systick_vector;
} vectors_t;

__attribute__ ((section("vectors"), used))
const vectors_t _switch_vectors = {
  __main_stack_end__, switch_entry_point, switch_loop, switch_loop,
  switch_loop, switch_loop, switch_loop, switch_loop,
  switch_loop, switch_loop, switch_loop, switch_loop,
  switch_loop, switch_loop, switch_loop, switch_loop,
};

