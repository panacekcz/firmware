/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/
__attribute__((always_inline))
static inline void set_bitband_bit(uint32_t const map, uint32_t const reg, uint8_t const bit){
	*(volatile uint32_t*)(PERIPHERAL_BITBAND + (map + reg) * 32 + bit * 4) = 1;
}

__attribute__((always_inline))
static inline uint32_t get_bitband_bit(uint32_t const map, uint32_t const reg, uint8_t const bit){
	return *(volatile uint32_t*)(PERIPHERAL_BITBAND + (map + reg) * 32 + bit * 4);
}

__attribute__((always_inline))
static inline void clear_peripheral(uint32_t const map, uint32_t const reg){
	*(volatile uint32_t*)(PERIPHERAL_MEMORY + map + reg) = 0;
}

__attribute__((always_inline))
static inline void write_peripheral(uint32_t const map, uint32_t const reg, uint32_t const value){
	*(volatile uint32_t*)(PERIPHERAL_MEMORY + map + reg) = value;
}

__attribute__((always_inline))
static inline void write_core(uint32_t const map, uint32_t const reg, uint32_t const value){
	*(volatile uint32_t*)(CORE_MEMORY + map + reg) = value;
}

__attribute__((always_inline))
static inline uint32_t get_peripheral(uint32_t const map, uint32_t const reg){
	return *(volatile uint32_t*)(PERIPHERAL_MEMORY + map + reg);
}
