/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SWITCH_H_
#define SWITCH_H_

#define SWITCH_ACTION(a) ((a)&0xFF) ///< Get switch action from reset reason.

/// @name Switch actions
/// @{
#define SWITCH_ACTION_APP 0x01 ///< Start application.
#define SWITCH_ACTION_LOADER 0x02 ///< Load and start loader.
#define SWITCH_ACTION_VERIFY 0x03 ///< Verify application.
#define SWITCH_ACTION_TIMER 0x04 ///< Wait.
/// @}

/// @name Reset reasons
/// @{
#define SWITCH_REASON_APPLICATION_REQUEST	0x0101 ///< User request (communication) to start app.
#define SWITCH_REASON_VERIFICATION_PASSED	0x0201 ///< Verification passed, can boot to app.
#define SWITCH_REASON_TIMEOUT				0x0301 ///< Timeout of timer.

#define SWITCH_REASON_LOADER_REQUEST		0x0102 ///< User request (communication) to loader.
#define SWITCH_REASON_VERIFICATION_FAILED	0x0202 ///< Verification failed, must boot to lader.
#define SWITCH_REASON_SOFTWARE_RESET		0x0402 ///< Software reset with unspecified reason.
#define SWITCH_REASON_INVALID_REASON 		0x0502 ///< Can be used to replace invalid reason code.
#define SWITCH_REASON_TIMER_RESET			0x0602 ///< Reset during timer.

#define SWITCH_REASON_VERIFICATION_REQUEST	0x0103 ///< User request (communication) to verify.
#define SWITCH_REASON_POWERON				0x0203 ///< Power on reset (default), verify app.

#define SWITCH_REASON_OTHER_RESET			0x0504 ///< Neither power on nor software reset (usually button reset).
/// @}

#endif /* SWITCH_H_ */
