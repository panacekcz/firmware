# Robot Firmware - Modified 2014 by Vlastimil Dort

TRGT = $(ARMGCC)/bin/arm-none-eabi-
CC   = $(TRGT)gcc
AR   = $(TRGT)ar

COPT = -c -mcpu=$(CPU) -ggdb -Os\
-fomit-frame-pointer -falign-functions=16  -ffunction-sections -fdata-sections -fno-common\
-Wall -Wextra -Wstrict-prototypes -Wa,-alms=build/switch.lst\
-DTHUMB_PRESENT -mno-thumb-interwork -DTHUMB_NO_INTERWORKING -mthumb -DTHUMB -I.

build:
	mkdir -p build

all: build build/switch.o

clean:
	rm -f build/switch.o build/switch.lst

build/switch.o: $(CFILE) mcu.h ../switch.h ../base.h ../functions.h ../io.h Makefile
	$(CC) $(COPT) -o $@ $<

.PHONY: all clean
