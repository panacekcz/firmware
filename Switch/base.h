/*
    ChibiOS/RT - Copyright (C) 2006,2007,2008,2009,2010,
                 2011,2012,2013 Giovanni Di Sirio.
    Robot Firmware - Modified 2014 by Vlastimil Dort

    This file is part of ChibiOS/RT.

    ChibiOS/RT is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    ChibiOS/RT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

extern const volatile uint32_t __loader_start__[];
extern volatile uint32_t _text[];
extern volatile uint32_t _etext[];

__attribute__((always_inline, noreturn))
static inline void start_at_addr(uint32_t const addr){
	//Move the vector table
	write_core(SCB, SCB_VTOR,addr);
	//Load the main stack pointer
	uint32_t const stack = ((const volatile uint32_t*)addr)[0];
	__asm__ volatile("msr msp, %0":: "r"(stack));
	__asm__ volatile("isb");
	//Jump to address
	uint32_t const code = ((const volatile uint32_t*)addr)[1];
	for(;;)
		__asm__ volatile("bx %0":: "r"(code));
}

/// Load the code of the loader from flash to ram.
__attribute__((always_inline))
static inline void load_loader(void){
	const volatile uint32_t* src = __loader_start__;
	volatile uint32_t* dst = _text;
	for(;(uint32_t)dst != (uint32_t)_etext; ++dst, ++src)
		*dst = *src;
}
