/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

extern volatile uint32_t __ram_start__[];
extern volatile uint32_t __ram_end__[];
extern const volatile uint32_t __app_start__[];
extern const volatile uint32_t __app_end__[];

__attribute__((always_inline))
static inline void disable_backup(void){
	//Disable access to backup registers
	clear_peripheral(PWR, PWR_CR);
	clear_peripheral(RCC, RCC_APB1ENR);
}

__attribute__((naked, noreturn))
void switch_loop(void){
	for(;;);
}

/// Check whether the data at the specified address
/// look like a valid program.
static int verify(const volatile uint32_t* const program){
	// Check that the main stack pointer points to memory
	uint32_t const stackaddr = program[0];
	if(stackaddr < (uint32_t)__ram_start__ || stackaddr > (uint32_t)__ram_end__)
		return 0;
	// Check that the reset vector is a THUMB pointer
	uint32_t resetvec = program[1];
	if(!(resetvec & 1))
		return 0;
	// Clear the THUMB bit
	resetvec ^= 1;

	// Check that the reset vector points to APP flash
	if(resetvec < (uint32_t)__app_start__ || resetvec >= (uint32_t)__app_end__)
		return 0;

	return 1;
}

/// Write a value to backup memory and perform software reset.
__attribute__((always_inline, noreturn))
static inline void reset_with_reason(uint32_t const reason){
	// Store the reason to the backup memory
	write_peripheral(BKP, BKP_RESETREASON, reason);
	// Issue system reset
	write_core(SCB, SCB_AIRCR, 0x05FA0004);
	// Enter infinite loop
	switch_loop();
}

/// Try to take the microcontroller into a state
/// very similar to reset state and then start code at a specified
/// address as if it would be on reset.
__attribute__((always_inline, noreturn))
static inline void uninit_start_at_addr(uint32_t const addr){
	// Disable backup region
	disable_backup();
	// Start code
	start_at_addr(addr);
}

/// Enable access to the backup memory.
__attribute__((always_inline))
static inline void enable_backup(void){
	// set PWREN in RCC_APB1ENR
	set_bitband_bit(RCC, RCC_APB1ENR, RCC_APB1ENR_PWREN);
	// set BKPEN in RCC_APB1ENR
	set_bitband_bit(RCC, RCC_APB1ENR, RCC_APB1ENR_BKPEN);
	// set DBP in PWR_CR
	set_bitband_bit(PWR, PWR_CR, PWR_CR_DBP);
}

__attribute__((always_inline, noreturn))
static inline void switch_procedure(void){
	//Reset scenarios:
	//- POWER ON:
	//   PORRSTF = 1, PINRSTF = 1, SFT_RSTF = 0, (BKP_RESETREASON = 0)
	//   Verify.
	//- SOFTWARE RESET
	//   PORRSTF = 0, PINRSTF = 1, SFT_RSTF = 1
	//   BKP_RESETREASON = verify req. Verify.
	//   BKP_RESETREASON = app req. Jump to app.
	//   BKP_RESETREASON = loader req. Load and jump to loader.
	//   BKP_RESETREASON = other. Load and jump to loader.
	//- BUTTON RESET
	//   PORRSTF = 0, PINRSTF = 1, SFT_RSTF = 0
	//   Timer.

	// Load reset flags
	uint32_t const is_poweron = get_bitband_bit(RCC, RCC_CSR, RCC_CSR_PORRSTF);
	uint32_t const is_software = get_bitband_bit(RCC, RCC_CSR, RCC_CSR_SFTRSTF);

	// Load reset reason from backup memory
	enable_backup();
	uint32_t reason = get_peripheral(BKP, BKP_RESETREASON);

	if(!is_software){
		// Not software reset - use preset reasons
		if(is_poweron){
			reason = SWITCH_REASON_POWERON;
		}
		else if(reason!=SWITCH_REASON_TIMER_RESET){
			//If it is not interrupting the timer, we don't care about the reason
			reason = SWITCH_REASON_OTHER_RESET;
		}
	}
	uint8_t const action = SWITCH_ACTION(reason);

	// Clear reset flags
	set_bitband_bit(RCC, RCC_CSR, RCC_CSR_RMVF);

	// Execute selected action
	if(action == SWITCH_ACTION_VERIFY){
		if(verify(__app_start__)){
			reset_with_reason(SWITCH_REASON_VERIFICATION_PASSED);
		}
		else{
			reset_with_reason(SWITCH_REASON_VERIFICATION_FAILED);
		}
	}
	else if(action == SWITCH_ACTION_TIMER){
		// Indicate that the reset interrupted the timer
		write_peripheral(BKP, BKP_RESETREASON, SWITCH_REASON_TIMER_RESET);
		//Now wait for some time (approx 0.5 sec)
		uint32_t i;
		for(i=0; i<2000000; ++i)
			__asm__ volatile("nop");
		// No action, boot into application
		reset_with_reason(SWITCH_REASON_TIMEOUT);
	}
	else{

		//Overwrite the reset reason with a default value
		write_peripheral(BKP, BKP_RESETREASON, SWITCH_REASON_SOFTWARE_RESET);
		//Store the reason so that the application can read it
		write_peripheral(BKP, BKP_LASTRESETREASON, reason);

		if(action == SWITCH_ACTION_APP){
			uninit_start_at_addr((uint32_t)__app_start__);
		}
		else{
			load_loader();
			uninit_start_at_addr((uint32_t)__ram_start__);
		}
	}
}
