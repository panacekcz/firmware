/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "module.h"
#include "communication.h"
#include "commserial.h"
#include "commtext.h"
#include "motors.h"
#include "motordriver.h"
#include "armboard.h"
#include "encoders.h"

void moduleMain(void){
	for(;;){
		motorSet(&motor_bottom, 50);
		motorSet(&motor_top, 50);

		chThdSleepMilliseconds(5000);
		palTogglePad(GPIOC, GPIOC_LED13);

		motorBrake(&motor_bottom);
		motorBrake(&motor_top);

		chThdSleepMilliseconds(5000);
		palTogglePad(GPIOC, GPIOC_LED13);

		motorSet(&motor_bottom, -50);
		motorSet(&motor_top, -50);

		chThdSleepMilliseconds(5000);
		palTogglePad(GPIOC, GPIOC_LED13);

		motorFree(&motor_bottom);
		motorFree(&motor_top);

		chThdSleepMilliseconds(5000);
		palTogglePad(GPIOC, GPIOC_LED13);
	}
}
