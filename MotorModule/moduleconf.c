/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "communication.h"
#include "module.h"
#include "commcan.h"
#include "encoders.h"
#include "motordriver.h"

const CommHandlerEntry comm_sdo_handlers[] = {
#if SHIELD_TOP_ID == SHIELD_ID_MOTOR && SHIELD_BOTTOM_ID == SHIELD_ID_MOTOR
	{0x0201, &moduleConfigObjectHandler,(uint32_t)&encoder_config},
	{0x0202, &moduleConfigObjectHandler,(uint32_t)&driver_config_object},
#endif

	// This object is needed by the loader
	{0x3E01, &moduleResetHandler,0},
};
const CommHandlerEntry comm_pdo_handlers[] = {
	{0x0381, &commEnableOutputHandler, 0},
#if SHIELD_TOP_ID == SHIELD_ID_MOTOR && SHIELD_BOTTOM_ID == SHIELD_ID_MOTOR
	{0x0184, &driverTargetHandler,0},
	{0x0189, &driverPwmHandler,0},
	{0x018B, &encoderPositionHandler,0},
	{0x018C, &encoderAngleHandler,0},
#endif
};
const uint32_t comm_sdo_handler_count=sizeof(comm_sdo_handlers)/sizeof(*comm_sdo_handlers);
const uint32_t comm_pdo_handler_count=sizeof(comm_pdo_handlers)/sizeof(*comm_pdo_handlers);

const CANFilter comm_can_filters[]={
	SIMPLE_2MASK_FILTER(0, 0x000, 0x000, 0x000, 0x000),
};
const uint32_t comm_can_filter_count=1;
