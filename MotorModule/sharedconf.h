/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SHAREDCONF_H_
#define SHAREDCONF_H_

#define HAL_USE_GPT 1
#define HAL_USE_PWM 1
#define HAL_USE_QEI 1

#define STM32_PWM_USE_ADVANCED 1

#define STM32_PWM_USE_TIM5 1
#define STM32_PWM_USE_TIM1 1

#define STM32_GPT_USE_TIM6 1

#define STM32_QEI_USE_TIM3 1
#define STM32_QEI_USE_TIM2 1

#define SHIELD_TOP_ID SHIELD_ID_MOTOR
#define SHIELD_BOTTOM_ID SHIELD_ID_MOTOR

#define COMM_ENABLE_CAN TRUE
#define COMM_HEARTBEAT 1000


#endif /* SHAREDCONF_H_ */
