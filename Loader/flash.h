/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FLASH_H_
#define FLASH_H_

#include "ch.h"

/// Clear all error flags set by previous operations.
void ldrFlashUnflagErrors(void);
/// Query error caused by flash protection.
/// @return Whether the last call to ::ldrFlashErase or ::ldrFlashWrite
///         attempted to modify protected flash region.
bool_t ldrFlashIsProtectError(void);
/// Query error during write.
/// @return Whether the last call to ::ldrFlashWrite ended with an error.
bool_t ldrFlashIsWriteError(void);
/// Perform flash write unlock.
/// You should call this function before starting to use the ::ldrFlashErase
/// and ::ldrFlashWrite functions. You can lock the flash again by ::ldrFlashLock.
void ldrFlashUnlock(void);
/// Lock flash write.
/// Disables writes to flash until ::ldrFlashUnlock is called again.
void ldrFlashLock(void);
/// Clear all state caused by previous flash operations.
void ldrFlashEndOperation(void);
/// Erase a fash page.
/// @param address Any address pointing into the page.
void ldrFlashErase(uint32_t address);
/// Write 2 bytes to the flash.
/// @param address Address of the low byte. Must be aligned.
/// @param data 16-bit value that should be written
void ldrFlashWrite(uint32_t address, uint16_t data);

#endif /* FLASH_H_ */
