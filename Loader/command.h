/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COMMAND_H_
#define COMMAND_H_

#include "commserial.h"

/// Execute serial line command.
void ldrSerialCommand(const CommSerial* serial);

void ldrSdoReset(CommHandlerEvent evt);
void ldrSdoAddress(CommHandlerEvent evt);
void ldrSdoCommand(CommHandlerEvent evt);
void ldrSdoData(CommHandlerEvent evt);
void ldrTestTokenHandler(CommHandlerEvent event);

#endif /* COMMAND_H_ */
