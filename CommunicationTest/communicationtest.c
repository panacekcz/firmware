/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "communicationtest.h"
#include "hal.h"

uint8_t buffers[17][17];
uint8_t test_objects[4][30];
Mutex test_mutex_1, test_mutex_2;

void testInit(void){
	chMtxInit(&test_mutex_1);
	chMtxInit(&test_mutex_2);
}

void testPdoHandler(CommHandlerEvent event){
	(void) event;
	// answer by sending 3 different pdos
	CommFrame cf;

	cf.length = 8;
	cf.data32[1] = comm_frame.data32[1];
	cf.data32[0] = 0x55FF0000;
	cf.data8[0] = 1;
	cf.sid = 0x180 + COMM_MODULE_ID;
	commPdoSend(&cf, TIME_IMMEDIATE);
	cf.data8[0] = 2;
	cf.sid = 0x380 + COMM_MODULE_ID;
	commPdoSend(&cf, TIME_IMMEDIATE);
	cf.data8[0] = 3;
	cf.sid = 0x580 - COMM_MODULE_ID;
	commPdoSend(&cf, TIME_IMMEDIATE);
}

static uint8_t testByte(uint32_t pos, uint16_t object_index, uint8_t object_subindex){
	return object_subindex + (object_index + pos * 33) * 33;
}

void testSdoReadOnlyHandler(CommHandlerEvent event){
	if(event == COMM_SDO_READ_START){
		comm_sdo.size = comm_sdo_handler->param;
	}
	else if(event == COMM_SDO_READ_DATA){
		for(uint8_t i = 0; i < comm_sdo.segment; ++i){
			comm_sdo.data8[i] = testByte(comm_sdo.index + i, comm_sdo.object_index, comm_sdo.object_subindex);
		}
	}
	else if(event == COMM_SDO_READ_END){

	}
	else if(event == COMM_SDO_READ_BREAK){
	}
	else{
		comm_sdo.error = COMM_SDO_ERROR_READONLY;
	}
}
void testSdoWriteOnlyHandler(CommHandlerEvent event){
	if(event == COMM_SDO_WRITE_START){
		if(comm_sdo.size > comm_sdo_handler->param)
			comm_sdo.error = COMM_SDO_ERROR_LENGTH_HIGH;
		else if(comm_sdo.size < comm_sdo_handler->param)
			comm_sdo.error = COMM_SDO_ERROR_LENGTH_LOW;
	}
	else if(event == COMM_SDO_WRITE_DATA){
		for(uint8_t i = 0; i < comm_sdo.segment; ++i){
			if(comm_sdo.data8[i] != testByte(comm_sdo.index + i, comm_sdo.object_index, comm_sdo.object_subindex))
				comm_sdo.error = COMM_SDO_ERROR_RANGE;
		}
	}
	else if(event == COMM_SDO_WRITE_END){

	}
	else if(event == COMM_SDO_WRITE_BREAK){
	}
	else{
		comm_sdo.error = COMM_SDO_ERROR_WRITEONLY;
	}
}
void testSdoReadWriteHandler(CommHandlerEvent event){
	if(event == COMM_SDO_WRITE_DATA){
		for(uint8_t i = 0; i < comm_sdo.segment; ++i){
			buffers[comm_sdo_handler->param][comm_sdo.index + i] = comm_sdo.data8[i];
		}
	}
	else if(event == COMM_SDO_READ_DATA){
		for(uint8_t i = 0; i < comm_sdo.segment;++i){
			comm_sdo.data8[i] = buffers[comm_sdo_handler->param][comm_sdo.index + i];
		}
	}
	else if(event == COMM_SDO_READ_START){
		comm_sdo.size = comm_sdo_handler->param;
	}
}

#if TEST_TOKEN
void testTokenHandler(CommHandlerEvent event){
	switch(event){
	case COMM_SDO_READ_START:
		comm_sdo.size = 4;
		break;
	case COMM_SDO_READ_DATA:
		comm_sdo.data32 = TEST_TOKEN;
	case COMM_SDO_READ_END:
	case COMM_SDO_READ_BREAK:
		break;
	default:
		comm_sdo.error = COMM_SDO_ERROR_READONLY;
		break;
	}
}
#endif

