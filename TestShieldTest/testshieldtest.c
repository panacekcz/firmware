/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "testshieldtest.h"
#include "armboard.h"
#include "hal.h"

static const uint32_t bases[] = {
#if SHIELD_TOP_ID == SHIELD_ID_TEST
	GPIOB_BASE,GPIOB_BASE,GPIOB_BASE,GPIOB_BASE,
	GPIOC_BASE,GPIOC_BASE,GPIOA_BASE,GPIOA_BASE,

	GPIOC_BASE,GPIOC_BASE,GPIOC_BASE,GPIOD_BASE,
	GPIOC_BASE,GPIOA_BASE,GPIOA_BASE,GPIOA_BASE,
#endif
#if SHIELD_BOTTOM_ID == SHIELD_ID_TEST
	GPIOB_BASE,GPIOB_BASE,GPIOB_BASE,GPIOB_BASE,
	GPIOC_BASE,GPIOC_BASE,GPIOB_BASE,GPIOA_BASE,

	GPIOA_BASE,GPIOB_BASE,GPIOB_BASE,GPIOC_BASE,
	GPIOA_BASE,GPIOB_BASE,GPIOB_BASE,GPIOA_BASE,
#endif
};
static const uint32_t offsets[] = {
#if SHIELD_TOP_ID == SHIELD_ID_TEST
	GPIOB_LEDTOPLEFT0,
	GPIOB_LEDTOPLEFT1,
	GPIOB_LEDTOPLEFT2,
	GPIOB_LEDTOPLEFT3,
	GPIOC_LEDTOPLEFT4,
	GPIOC_LEDTOPLEFT5,
	GPIOA_LEDTOPLEFT6,
	GPIOA_LEDTOPLEFT7,

	GPIOC_LEDTOPRIGHT0,
	GPIOC_LEDTOPRIGHT1,
	GPIOC_LEDTOPRIGHT2,
	GPIOD_LEDTOPRIGHT3,
	GPIOC_LEDTOPRIGHT4,
	GPIOA_LEDTOPRIGHT5,
	GPIOA_LEDTOPRIGHT6,
	GPIOA_LEDTOPRIGHT7,
#endif
#if SHIELD_BOTTOM_ID == SHIELD_ID_TEST
	GPIOB_LEDBOTTOMLEFT0,
	GPIOB_LEDBOTTOMLEFT1,
	GPIOB_LEDBOTTOMLEFT2,
	GPIOB_LEDBOTTOMLEFT3,
	GPIOC_LEDBOTTOMLEFT4,
	GPIOC_LEDBOTTOMLEFT5,
	GPIOB_LEDBOTTOMLEFT6,
	GPIOA_LEDBOTTOMLEFT7,

	GPIOA_LEDBOTTOMRIGHT0,
	GPIOB_LEDBOTTOMRIGHT1,
	GPIOB_LEDBOTTOMRIGHT2,
	GPIOC_LEDBOTTOMRIGHT3,
	GPIOA_LEDBOTTOMRIGHT4,
	GPIOB_LEDBOTTOMRIGHT5,
	GPIOB_LEDBOTTOMRIGHT6,
	GPIOA_LEDBOTTOMRIGHT7,
#endif
};

enum{
	LED_COUNT = sizeof(bases) / sizeof(*bases)
};

void testShieldShow(uint32_t value){
	int i;
	for(i = 0; i < LED_COUNT; ++i){
		palWritePad(((GPIO_TypeDef *)bases[i]), offsets[i], (value & (1 << i)) != 0);
	}
}

void testShieldTestLeds(void){
	int i;
	for(i = 0; i < LED_COUNT; ++i){
		palSetPad(((GPIO_TypeDef *)bases[i]), offsets[i]);
		chThdSleepMilliseconds(50);
		palClearPad(((GPIO_TypeDef *)bases[i]) , offsets[i]);
	}
}

void testShieldTestMain(void){
	for(;;)
		testShieldTestLeds();
}
