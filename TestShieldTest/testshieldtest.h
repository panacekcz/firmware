/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TESTSHIELDTEST_H_
#define TESTSHIELDTEST_H_

#include "ch.h"

/// Blink each LED on the TestShield once.
void testShieldTestLeds(void);

/// Blink all LEDs on the TestShield in a loop.
void testShieldTestMain(void);

/// Show a 32-bit or 16-bit value on the TestShield.
void testShieldShow(uint32_t value);

#endif /* TESTSHIELDTEST_H_ */
