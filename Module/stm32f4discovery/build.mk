# Robot Firmware - Modified 2014 by Vlastimil Dort

#Include referenced makefiles
include $(CHIBIOS)/boards/ST_STM32F4_DISCOVERY/board.mk
include $(CHIBIOS)/os/hal/platforms/STM32F4xx/platform.mk
include $(CHIBIOS)/os/hal/hal.mk
include $(CHIBIOS)/os/ports/GCC/ARMCMx/STM32F4xx/port.mk
include $(CHIBIOS)/os/kernel/kernel.mk

LDSCRIPT= $(PORTLD)/STM32F407xG.ld
DDEFS =

DINCDIR = $(WORKSPACE)/Module/stm32f407
RESETSRC = $(WORKSPACE)/Module/stm32f407/reset.c
