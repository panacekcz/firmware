# Robot Firmware - Modified 2014 by Vlastimil Dort

#Include referenced makefiles
include $(CHIBIOS)/boards/ST_STM32VL_DISCOVERY/board.mk
include $(CHIBIOS)/os/hal/platforms/STM32F1xx/platform.mk
include $(CHIBIOS)/os/hal/hal.mk
include $(CHIBIOS)/os/ports/GCC/ARMCMx/STM32F1xx/port.mk
include $(CHIBIOS)/os/kernel/kernel.mk

LDSCRIPT= $(PORTLD)/STM32F100xB.ld
DDEFS =

DINCDIR = $(WORKSPACE)/Module/stm32f100
RESETSRC = $(WORKSPACE)/Module/stm32f1xx/reset.c
