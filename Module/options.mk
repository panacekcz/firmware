# Robot Firmware - Modified 2014 by Vlastimil Dort

PROJECT = ch

# Compiler options here.
ifeq ($(USE_OPT),)
ifeq ($(USE_DEBUG),yes)
  USE_OPT = -O0 -ggdb -fomit-frame-pointer -falign-functions=16
else
  USE_OPT = -O2 -fomit-frame-pointer -falign-functions=16
endif
endif
# C specific options here (added to USE_OPT).
ifeq ($(USE_COPT),)
ifeq ($(USE_ISO11),yes)
  USE_COPT = -std=gnu11
else
  USE_COPT = -std=gnu99
endif
endif
# C++ specific options here (added to USE_OPT).
ifeq ($(USE_CPPOPT),)
ifeq ($(USE_ISO11),yes)
  USE_CPPOPT = -fno-rtti -std=gnu++11
else
  USE_CPPOPT = -fno-rtti
endif
endif
# Enable this if you want the linker to remove unused code and data
ifeq ($(USE_LINK_GC),)
  USE_LINK_GC = yes
endif
# Enable this if you want to see the full log while compiling.
ifeq ($(USE_VERBOSE_COMPILE),)
  USE_VERBOSE_COMPILE = no
endif
#Module always in thumb mode
USE_THUMB = yes