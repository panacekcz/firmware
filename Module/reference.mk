# Robot Firmware - 2014 Vlastimil Dort

# This file should be included in the makefiles of modules.

# Load saved configuration values
-include local.mk

# Use the saved configuration values for options not specified
# on the command line or in the environment. 
$(foreach x,$(CONFIGS),$(eval CONF_$(x)?=$$(LOCAL_$(x))))

include $(CONF_BUILD)/build.mk
include $(WORKSPACE)/Module/$(MODULE_CONF)/build.mk
include $(WORKSPACE)/Module/options.mk