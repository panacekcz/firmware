# Robot Firmware - Modified 2014 by Vlastimil Dort

#Include referenced makefiles
include $(WORKSPACE)/ARMBoard/stm32f107/board.mk
include $(CHIBIOS)/os/hal/platforms/STM32F1xx/platform_f105_f107.mk
include $(CHIBIOS)/os/hal/hal.mk
include $(CHIBIOS)/os/ports/GCC/ARMCMx/STM32F1xx/port.mk
include $(CHIBIOS)/os/kernel/kernel.mk

# Use modified linker script if not debugging
ifeq ($(USE_LOADER),yes)
LDSCRIPT= $(WORKSPACE)/Module/stm32f107/loadable.ld
DDEFS = -DCORTEX_VTOR_INIT=0x08004000
else
LDSCRIPT= $(PORTLD)/STM32F107xC.ld
DDEFS =
endif

DINCDIR = $(WORKSPACE)/Module/stm32f107
RESETSRC = $(WORKSPACE)/Module/stm32f1xx/reset.c