/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "sharp.h"
#include "hal.h"
#include "armboard.h"

#if SHIELD_TOP_ID == SHIELD_ID_IR && IRSHIELD_TOP_SHARP
//PC4 = ADC12_IN14
const ADCConversionGroup sharp_top_group={
	FALSE, // Not circular
	1, // One channel
	NULL, NULL,// Callbacks
	0, 0, // Error registers
	ADC_SMPR1_SMP_AN14(ADC_SAMPLE_1P5), 0, //TODO: sample times
	ADC_SQR1_NUM_CH(1),
	0,
	ADC_SQR3_SQ1_N(ADC_CHANNEL_IN14)
};
#endif

#if SHIELD_BOTTOM_ID == SHIELD_ID_IR && IRSHIELD_BOTTOM_SHARP
//PC2 = ADC12_IN12
const ADCConversionGroup sharp_bottom_group={
	FALSE, // Not circular
	1, // One channel
	NULL, NULL,// Callbacks
	0, 0, // Error registers
	ADC_SMPR1_SMP_AN12(ADC_SAMPLE_1P5), 0, //TODO: sample times
	ADC_SQR1_NUM_CH(1),
	0,
	ADC_SQR3_SQ1_N(ADC_CHANNEL_IN12)
};
#endif

void sharpStart(void){
#if (SHIELD_TOP_ID == SHIELD_ID_IR && IRSHIELD_TOP_SHARP) || (SHIELD_BOTTOM_ID == SHIELD_ID_IR && IRSHIELD_BOTTOM_SHARP)
	adcStart(&ADCD1, NULL);
#endif
}

#if (SHIELD_TOP_ID == SHIELD_ID_IR && IRSHIELD_TOP_SHARP) || (SHIELD_BOTTOM_ID == SHIELD_ID_IR && IRSHIELD_BOTTOM_SHARP)
static void sharpConvertTransmit(const ADCConversionGroup* group, uint16_t pdo_id){
	CommFrame frame;
	adcConvert(&ADCD1, group, &frame.data16[0], 1);
	frame.length = 2;
	frame.sid = 0x180 + pdo_id;
	commPdoSend(&frame, TIME_IMMEDIATE);
}
#endif

void sharpTransmit(void){
#if SHIELD_TOP_ID == SHIELD_ID_IR && IRSHIELD_TOP_SHARP
	sharpConvertTransmit(&sharp_top_group, SHARP_TOP_PDO);
#endif
#if SHIELD_BOTTOM_ID == SHIELD_ID_IR && IRSHIELD_BOTTOM_SHARP
	sharpConvertTransmit(&sharp_bottom_group, SHARP_BOTTOM_PDO);
#endif
}
