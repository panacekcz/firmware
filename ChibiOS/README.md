# Eclipse project for indexing ChibiOS sources

This directory does not contain ChibiOS/RT, you must download it from <http://chibios.org>.

This Eclipse project exists to allow the Eclipse CDT indexer to find the right files from ChibiOS/RT.
It also contains drivers which are not included in the official ChibiOS/RT releases (located in `thirdparty` directory).

The ChibiOS/RT sources are located using "linked resources" in Eclipse.
In Window > Preferences > General > Workspace > Linked resources set variable CHIBIOS to the path where ChibiOS/RT is installed.
You should also set the `CHIBIOS` environment variable to the same value before starting Eclipse.