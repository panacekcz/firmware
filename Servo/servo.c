/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "servo.h"
#include "hal.h"
#include "armboard.h"

#if SHIELD_TOP_ID == SHIELD_ID_SERVO || SHIELD_BOTTOM_ID == SHIELD_ID_SERVO

static const PWMConfig pwm_2_config = {
	SERVO_PWM_TIMER_FREQ,
	SERVO_PWM_PERIOD,
	NULL,
	{
		{PWM_OUTPUT_ACTIVE_LOW, NULL},//TIM2_CH1 on PA15 (BOTTOM-A)
		{PWM_OUTPUT_ACTIVE_LOW, NULL},//TIM2_CH2 on PB3 (BOTTOM-B)
		{PWM_OUTPUT_DISABLED, NULL},
		{PWM_OUTPUT_DISABLED, NULL}
	},
	0,
	0,
};
static const PWMConfig pwm_3_config = {
	SERVO_PWM_TIMER_FREQ,
	SERVO_PWM_PERIOD,
	NULL,
	{
		{PWM_OUTPUT_ACTIVE_LOW, NULL},//TIM3_CH1 on PA6 (TOP-A)
		{PWM_OUTPUT_ACTIVE_LOW, NULL},//TIM3_CH2 on PA7 (TOP-B)
		{PWM_OUTPUT_ACTIVE_LOW, NULL},//TIM3_CH3 on PB0 (BOTTOM-D)
		{PWM_OUTPUT_ACTIVE_LOW, NULL},//TIM3_CH4 on PB1 (BOTTOM-C)
	},
	0,
	0,
};
static const PWMConfig pwm_5_config = {
	SERVO_PWM_TIMER_FREQ,
	SERVO_PWM_PERIOD,
	NULL,
	{
		{PWM_OUTPUT_ACTIVE_LOW, NULL},//TIM5_CH1 on PA0 (TOP-C)
		{PWM_OUTPUT_ACTIVE_LOW, NULL},//TIM5_CH2 on PA1 (TOP-D)
		{PWM_OUTPUT_DISABLED, NULL},
		{PWM_OUTPUT_DISABLED, NULL}
	},
	0,
	0,
};
#endif

#if SHIELD_TOP_ID == SHIELD_ID_SERVO
void servoTopHandler(CommHandlerEvent event){
	(void)event;
	pwmEnableChannel(&PWMD3, 0, comm_frame.data8[0]);
	pwmEnableChannel(&PWMD3, 1, comm_frame.data8[1]);
	pwmEnableChannel(&PWMD5, 0, comm_frame.data8[2]);
	pwmEnableChannel(&PWMD5, 1, comm_frame.data8[3]);
}
#endif
#if SHIELD_BOTTOM_ID == SHIELD_ID_SERVO
void servoBottomHandler(CommHandlerEvent event){
	(void)event;
	pwmEnableChannel(&PWMD2, 0, comm_frame.data8[0]);
	pwmEnableChannel(&PWMD2, 1, comm_frame.data8[1]);
	pwmEnableChannel(&PWMD3, 3, comm_frame.data8[2]);
	pwmEnableChannel(&PWMD3, 2, comm_frame.data8[3]);
}
#endif
void servoStart(void){
#if SHIELD_TOP_ID == SHIELD_ID_SERVO || SHIELD_BOTTOM_ID == SHIELD_ID_SERVO
	pwmStart(&PWMD2, &pwm_2_config);
	pwmStart(&PWMD3, &pwm_3_config);
	pwmStart(&PWMD5, &pwm_5_config);
#endif
}
