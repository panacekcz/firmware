/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SERVO_H_
#define SERVO_H_

#include "module.h"
#include "ch.h"

#if !defined(SERVO_PWM_TIMER_FREQ)
/// Frequency of the PWM timers.
#define SERVO_PWM_TIMER_FREQ 100000
#endif

#if !defined(SERVO_PWM_PERIOD)
/// Period of the PWM timers, in ticks specified by the timer frequency.
#define SERVO_PWM_PERIOD 1000
#endif

/// Enable servo control.
void servoStart(void);

void servoTopHandler(CommHandlerEvent event);
void servoBottomHandler(CommHandlerEvent event);

#endif
