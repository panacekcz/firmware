/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SERVOSHIELD_H_
#define SERVOSHIELD_H_

#if SHIELD_TOP_ID == SHIELD_ID_SERVO
/// @name Pin initialization
/// @{

//TODO:
#define VAL_TOP_GPIOACR  0x00000000BB0004BBull
#define VAL_TOP_GPIOAODR 0b0000000000000000
#define VAL_TOP_GPIOBCR  0x0044440000000000ull
#define VAL_TOP_GPIOBODR 0b0000000000000000
#define VAL_TOP_GPIOCCR  0x0004440000440040ull
#define VAL_TOP_GPIOCODR 0b0000000000000000
#define VAL_TOP_GPIODCR  0x0000000000000400ull
#define VAL_TOP_GPIODODR 0b0000000000000000
/// @}
#endif

#if SHIELD_BOTTOM_ID == SHIELD_ID_SERVO

/// @name Pin initialization
/// @{

//TODO:
#define VAL_BOTTOM_GPIOACR  0xB000000000444000ull
#define VAL_BOTTOM_GPIOAODR 0b0000000000000000
#define VAL_BOTTOM_GPIOBCR  0x440000004444B0BBull
#define VAL_BOTTOM_GPIOBODR 0b0000000000000000
#define VAL_BOTTOM_GPIOCCR  0x0000000004004400ull
#define VAL_BOTTOM_GPIOCODR 0b0000000000000000
#define VAL_BOTTOM_GPIODCR  0x0000000000000000ull
#define VAL_BOTTOM_GPIODODR 0b0000000000000000

#ifdef BOARD_C_

#define SHIELD_INIT_BOTTOM shieldBottomInit

static void shieldBottomInit(void) {
	// Remap TIM2 to A15/B3
	AFIO->MAPR |= AFIO_MAPR_TIM2_REMAP_FULLREMAP;
}

#endif

/// @}
#endif

#endif /* SERVOSHIELD_H_ */
