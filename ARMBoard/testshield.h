/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SHIELD_TESTSHIELD_H_
#define SHIELD_TESTSHIELD_H_

/** @file testshield.h
 * Test shield. Can be both on top and bottom.
 * Supports 16 LEDs and USART1.
 */

#if SHIELD_TOP_ID == SHIELD_ID_TEST

/// @name Top test shield configuration
/// @{
#if !defined(SHIELD_TEST_TOP_INIT_LEDS) || defined(__DOXYGEN__)
/// Initialize pins as outputs to drive LEDs
#define SHIELD_TEST_TOP_INIT_LEDS FALSE
#endif

#if !defined(SHIELD_TEST_TOP_INIT_LEDS) || defined(__DOXYGEN__)
/// Initialize UART pins
#define SHIELD_TEST_TOP_INIT_UART FALSE
#endif

/// @}

/// @name Pins
/// @{
#define GPIOB_LEDB13 13
#define GPIOB_LEDB12 12
#define GPIOB_LEDB11 11
#define GPIOB_LEDB10 10
#define GPIOC_LEDC5 5
#define GPIOC_LEDC4 4
#define GPIOA_LEDA7 7
#define GPIOA_LEDA6 6

#define GPIOB_LEDTOPLEFT0 13
#define GPIOB_LEDTOPLEFT1 12
#define GPIOB_LEDTOPLEFT2 11
#define GPIOB_LEDTOPLEFT3 10
#define GPIOC_LEDTOPLEFT4 5
#define GPIOC_LEDTOPLEFT5 4
#define GPIOA_LEDTOPLEFT6 7
#define GPIOA_LEDTOPLEFT7 6

#define GPIOC_LEDC10 10
#define GPIOC_LEDC11 11
#define GPIOC_LEDC12 12
#define GPIOD_LEDD2 2
#define GPIOC_LEDC1 1
#define GPIOA_LEDA0 0
#define GPIOA_LEDA1 1
#define GPIOA_LEDA2 2

#define GPIOC_LEDTOPRIGHT0 10
#define GPIOC_LEDTOPRIGHT1 11
#define GPIOC_LEDTOPRIGHT2 12
#define GPIOD_LEDTOPRIGHT3 2
#define GPIOC_LEDTOPRIGHT4 1
#define GPIOA_LEDTOPRIGHT5 0
#define GPIOA_LEDTOPRIGHT6 1
#define GPIOA_LEDTOPRIGHT7 2
/// @}

/// @name Pin initialization
/// @{

#if SHIELD_TEST_TOP_INIT_LEDS

#define VAL_TOP_GPIOACR  0x0000000033000333ull
#define VAL_TOP_GPIOAODR 0b0000000000000000
#define VAL_TOP_GPIOBCR  0x0033330000000000ull
#define VAL_TOP_GPIOBODR 0b0000000000000000
#define VAL_TOP_GPIOCCR  0x0003330000330030ull
#define VAL_TOP_GPIOCODR 0b0000000000000000
#define VAL_TOP_GPIODCR  0x0000000000000300ull
#define VAL_TOP_GPIODODR 0b0000000000000000

#else
#error Not supported
/// TODO: other initialization choices
#endif
/// @}

#endif

#if SHIELD_BOTTOM_ID == SHIELD_ID_TEST
/// @name Pins
/// @{

#define GPIOB_LEDB4 4
#define GPIOB_LEDB5 5
#define GPIOB_LEDB7 7
#define GPIOB_LEDB6 6
#define GPIOC_LEDC3 3
#define GPIOC_LEDC2 2
#define GPIOB_LEDB3 3
#define GPIOA_LEDA15 15

#define GPIOB_LEDBOTTOMLEFT0 4
#define GPIOB_LEDBOTTOMLEFT1 5
#define GPIOB_LEDBOTTOMLEFT2 7
#define GPIOB_LEDBOTTOMLEFT3 6
#define GPIOC_LEDBOTTOMLEFT4 3
#define GPIOC_LEDBOTTOMLEFT5 2
#define GPIOB_LEDBOTTOMLEFT6 3
#define GPIOA_LEDBOTTOMLEFT7 15

#define GPIOA_LEDA5 5
#define GPIOB_LEDB14 14
#define GPIOB_LEDB15 15
#define GPIOC_LEDC6 6
#define GPIOA_LEDA4 4
#define GPIOB_LEDB1 1
#define GPIOB_LEDB0 0
#define GPIOA_LEDA3 3

#define GPIOA_LEDBOTTOMRIGHT0 5
#define GPIOB_LEDBOTTOMRIGHT1 14
#define GPIOB_LEDBOTTOMRIGHT2 15
#define GPIOC_LEDBOTTOMRIGHT3 6
#define GPIOA_LEDBOTTOMRIGHT4 4
#define GPIOB_LEDBOTTOMRIGHT5 1
#define GPIOB_LEDBOTTOMRIGHT6 0
#define GPIOA_LEDBOTTOMRIGHT7 3

/// @}

/// @name Pin initialization
/// @{

#if SHIELD_TEST_BOTTOM_INIT_LEDS
#define VAL_BOTTOM_GPIOACR  0x3000000000333000ull
#define VAL_BOTTOM_GPIOAODR 0b0000000000000000
#define VAL_BOTTOM_GPIOBCR  0x3300000033333033ull
#define VAL_BOTTOM_GPIOBODR 0b0000000000000000
#define VAL_BOTTOM_GPIOCCR  0x0000000003003300ull
#define VAL_BOTTOM_GPIOCODR 0b0000000000000000
#define VAL_BOTTOM_GPIODCR  0x0000000000000000ull
#define VAL_BOTTOM_GPIODODR 0b0000000000000000

#elif SHIELD_TEST_BOTTOM_INIT_UART

#define VAL_BOTTOM_GPIOACR  0x4000000000444000ull
#define VAL_BOTTOM_GPIOAODR 0b0000000000000000
#define VAL_BOTTOM_GPIOBCR  0x440000004B444044ull
#define VAL_BOTTOM_GPIOBODR 0b0000000000000000
#define VAL_BOTTOM_GPIOCCR  0x0000000004004400ull
#define VAL_BOTTOM_GPIOCODR 0b0000000000000000
#define VAL_BOTTOM_GPIODCR  0x0000000000000000ull
#define VAL_BOTTOM_GPIODODR 0b0000000000000000

#ifdef BOARD_C_

#define SHIELD_INIT_BOTTOM shieldBottomInit

static void shieldBottomInit(void) {
	AFIO->MAPR |= AFIO_MAPR_USART1_REMAP;
}

#endif

#else
#error Not supported
/// TODO: other initialization choices
#endif
/// @}

#endif

#endif /* SHIELD_TESTSHIELD_H_ */
