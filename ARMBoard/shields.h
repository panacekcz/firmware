/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file shields.h
 * This file contains definitions of identifiers for shields to
 * allow their selection before including armboard.h
 */

#ifndef SHIELDS_H_
#define SHIELDS_H_

#define SHIELD_ID_NONE 0x00000000
#define SHIELD_ID_TEST 0x00000001
#define SHIELD_ID_IR	0x00000002
#define SHIELD_ID_MOTOR 0x00000003
#define SHIELD_ID_SERVO 0x00000004

#endif /* SHIELDS_H_ */
