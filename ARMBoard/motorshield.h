/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MOTORSHIELD_H_
#define MOTORSHIELD_H_

#if SHIELD_TOP_ID == SHIELD_ID_MOTOR

/// Encoder index input.
#define GPIOB_ENC_I_TOP 11
/// Encoder A.
#define GPIOA_ENC_A_TOP 6
/// Encoder B.
#define GPIOA_ENC_B_TOP 7

/// Motor dir 1. Push pull output.
#define GPIOD_MOTOR_DIR1_TOP 2
/// Motor dir 2. Push pull output.
#define GPIOC_MOTOR_DIR2_TOP 5
/// Motor dir 3. Push pull output.
#define GPIOC_MOTOR_DIR3_TOP 4

/// Motor pwm 1. Push pull alternate output.
#define GPIOA_MOTOR_PWM1_TOP 2
/// Motor pwm 2. Push pull alternate output.
#define GPIOA_MOTOR_PWM2_TOP 1
/// Motor pwm 3. Push pull alternate output.
#define GPIOA_MOTOR_PWM3_TOP 0

/// Motor sense
#define GPIOC_MOTOR_SENSE_TOP 1
/// Motor fault
#define GPIOB_MOTOR_FAULT_TOP 10


#define VAL_TOP_GPIOACR  0x0000000044000BBBull
#define VAL_TOP_GPIOAODR 0b0000000000000000
#define VAL_TOP_GPIOBCR  0x0044440000000000ull
#define VAL_TOP_GPIOBODR 0b0000000000000000
#define VAL_TOP_GPIOCCR  0x0004440000330040ull
#define VAL_TOP_GPIOCODR 0b0000000000000000
#define VAL_TOP_GPIODCR  0x0000000000000300ull
#define VAL_TOP_GPIODODR 0b0000000000000000

#endif

#if SHIELD_BOTTOM_ID == SHIELD_ID_MOTOR

/// Encoder index input.
#define GPIOB_ENC_I_BOTTOM 7
/// Encoder A.
#define GPIOA_ENC_A_BOTTOM 15
/// Encoder B.
#define GPIOB_ENC_B_BOTTOM 3

/// Motor dir 1. Push pull output.
#define GPIOC_MOTOR_DIR1_BOTTOM 6
/// Motor dir 2. Push pull output.
#define GPIOC_MOTOR_DIR2_BOTTOM 3
/// Motor dir 3. Push pull output.
#define GPIOC_MOTOR_DIR3_BOTTOM 2

/// Motor pwm 1. Push pull alternate output.
#define GPIOA_MOTOR_PWM1_BOTTOM 3
/// Motor pwm 2. Push pull alternate output.
#define GPIOB_MOTOR_PWM2_BOTTOM 0
/// Motor pwm 3. Push pull alternate output.
#define GPIOB_MOTOR_PWM3_BOTTOM 1

/// Motor sense
#define GPIOA_MOTOR_SENSE_BOTTOM 4
/// Motor fault
#define GPIOB_MOTOR_FAULT_BOTTOM 6

#define VAL_BOTTOM_GPIOACR  0x400000000044B000ull
#define VAL_BOTTOM_GPIOAODR 0b0000000000000000
#define VAL_BOTTOM_GPIOBCR  0x44000000444440BBull
#define VAL_BOTTOM_GPIOBODR 0b0000000000000000
#define VAL_BOTTOM_GPIOCCR  0x0000000003003300ull
#define VAL_BOTTOM_GPIOCODR 0b0000000000000000
#define VAL_BOTTOM_GPIODCR  0x0000000000000000ull
#define VAL_BOTTOM_GPIODODR 0b0000000000000000

#ifdef BOARD_C_

#define SHIELD_INIT_BOTTOM shieldBottomInit

static void shieldBottomInit(void) {
	// Remap TIM2 to A15/B3
	// Remap TIM1 to B0/B1
	AFIO->MAPR |= AFIO_MAPR_TIM2_REMAP_FULLREMAP | AFIO_MAPR_TIM1_REMAP_PARTIALREMAP;

}

#endif


#endif

#endif /* MOTORSHIELD_H_ */
