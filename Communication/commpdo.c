/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @file commpdo.c
 * Implementation of PDO communication compatible with CanOpen.
 */

#include "communication.h"
#include "commbuffer.h"

#if COMM_DIAGNOSTICS
uint32_t comm_pdo_missed_frames;
#endif
const CommHandlerEntry* comm_pdo_handler;

static inline uint16_t commCurrentPdoId(void){
	return comm_frame.sid - 0x180;
}

static const CommHandlerEntry* commFindPdoHandler(uint16_t pdoId){
	uint32_t i;
	for(i = 0; i < comm_pdo_handler_count; ++i)
		if(comm_pdo_handlers[i].id == pdoId)
			return comm_pdo_handlers + i;
	return NULL;
}

void commHandlePdoFrame(void){
	comm_pdo_handler = commFindPdoHandler(commCurrentPdoId());
	if(comm_pdo_handler != NULL)
		comm_pdo_handler->handler(COMM_PDO);
}

/**
 * Enqueue the frame to be sent as a PDO.
 * The caller can modify the frame after this function returns.
 *
 * If the buffer is full, this function waits the specified timeout and
 * after that increments an error counter.
 *
 */
msg_t commPdoSend(const CommFrame* fr, systime_t timeout){
	CommBufferItem* item = commBufferPushLockTimeout(&comm_buffer, timeout);
	if(item!=NULL){
		item->frame = *fr;
		item->source = COMM_SOURCE_MODULE;
		commBufferPushUnlock(&comm_buffer);
		return Q_OK;
	}
	else{
#if COMM_DIAGNOSTICS
		chSysLock();
		comm_pdo_missed_frames++;
		chSysUnlock();
#endif
		return Q_FULL;
	}
}

msg_t commPdoSendI(const CommFrame* fr){
	CommBufferItem* item = commBufferPushLockI(&comm_buffer);
	if(item!=NULL){
		item->frame = *fr;
		item->source = COMM_SOURCE_MODULE;
		commBufferPushUnlockI(&comm_buffer);
		return Q_OK;
	}
	else{
#if COMM_DIAGNOSTICS
		comm_pdo_missed_frames++;
#endif
		return Q_FULL;
	}
}
