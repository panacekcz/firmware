/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COMMCAN_H_
#define COMMCAN_H_

#include "hal.h"

#define SIMPLE_4ID_FILTER(fi, id1, id2, id3, id4) \
		{fi, 1, 0, 0, ((id1)<<5) | ((id2)<<21), ((id3)<<5) | ((id4)<<21)}
#define SIMPLE_2ID_FILTER(fi, id1, id2) \
		{fi, 1, 1, 0, (id1)<<21,(id2)<<21}
#define SIMPLE_2MASK_FILTER(fi, mask1, id1, mask2, id2) \
		{fi, 0, 0, 0, ((id1)<<5) | ((mask1)<<21), ((id2)<<5) | ((mask2)<<21)}
#define SIMPLE_1MASK_FILTER(fi, mask1, id1) \
		{fi, 0, 1, 0, (id1)<<21,(mask1)<<21}

#if HAL_USE_CAN

extern const CANFilter comm_can_filters[];
extern const uint32_t comm_can_filter_count;

#endif

#endif
