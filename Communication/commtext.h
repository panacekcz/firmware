/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COMMTEXT_H_
#define COMMTEXT_H_

#include "ch.h"

/// Convert a 4-bit number into a hexadecimal character.
char commTextToHexDigit(uint8_t p);

/// Convert a hexadecimal character to a 4-bit number.
uint8_t commTextFromHexDigit(char p);

/// Print string to a stream. Newline is not added.
void prints(BaseSequentialStream* ch, const char* str);

/// Print a newline to a stream.
void println(BaseSequentialStream* ch);

/// Print string and a newline to a stream.
void printsln(BaseSequentialStream* ch, const char* str);

/// Print one hexadecimal digit of the number to a stream.
void printx1(BaseSequentialStream* ch, uint8_t x1);

/// Print 2 hexadecimal digits of the number to a stream.
void printx2(BaseSequentialStream* ch, uint8_t x2);

/// Print 4 hexadecimal digits of the number to a stream.
void printx4(BaseSequentialStream* ch, uint16_t x4);

/// Print 8 hexadecimal digits of the number to a stream.
void printx8(BaseSequentialStream* ch, uint32_t x8);

/// Print @p xn hexadecimal digits of the number to a stream.
void printxn(BaseSequentialStream* ch, uint32_t xn, uint32_t n);


#endif /* COMMTEXT_H_ */
