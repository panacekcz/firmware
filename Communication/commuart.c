/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "commserial.h"
#include "communication.h"

#if COMM_ENABLE_UART1

SerialDriver SD1;
static CommSerialLine comm_uart1_line;
static const SerialConfig SD1Config = {9600, 0, 0, 0};
static const CommSerial comm_uart1_serial = {
	&comm_uart1_line, COMM_SOURCE_UART1, (BaseChannel*)&SD1
};

void commUart1ReceiveAll(void){
	commSerialReceiveAll(&comm_uart1_serial);
}

void commUart1Transmit(void){
	commSerialTransmit(&comm_uart1_serial);
}

/// Initialize UART1 for Communication
void commUart1Init(void){
	sdObjectInit(&SD1, NULL, NULL);
	sdStart(&SD1, &SD1Config);
}

#endif

#if COMM_ENABLE_UART2

static CommSerialLine comm_uart2_line;
static const SerialConfig SD2Config = {38400, 0, 0, 0};
const CommSerial comm_uart2_serial = {
	&comm_uart2_line, COMM_SOURCE_UART2, (BaseChannel*)&SD2
};

void commUart2ReceiveAll(void){
	commSerialReceiveAll(&comm_uart2_serial);
}

void commUart2Transmit(void){
	commSerialTransmit(&comm_uart2_serial);
}

/// Initialize UART2 for Communication
void commUart2Init(void){
	sdStart(&SD2, &SD2Config);
}

#endif
