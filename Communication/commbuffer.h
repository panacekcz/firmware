/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef COMMBUFFER_H_
#define COMMBUFFER_H_

#include "ch.h"
#include "communication.h"

#if defined(__DOXYGEN__) || !defined(COMM_BUFFER_SIZE)
/// Number of frames in a buffer. Can be overridden.
#define COMM_BUFFER_SIZE 20
#endif

/// Item representing a frame in a buffer.
typedef struct {
	/// The frame represented by the item.
	CommFrame frame;
	/// The source of the frame.
	uint8_t source;
}CommBufferItem;

typedef struct{
	/// Cyclic buffer containing frames.
	CommBufferItem buffer[COMM_BUFFER_SIZE];
	/// Pointer to the begin into the buffer (read side).
	CommBufferItem* begin;
	/// Pointer to the end into the buffer (write side).
	CommBufferItem* end;
	/// Semaphore counting empty slots.
	Semaphore empty_sem;
	/// Event signaled after pushing an item.
	EventSource event;

} CommBuffer;

/// Initialize communication buffer.
/// Can be invoked before the kernel is initialized.
void commBufferInit(CommBuffer* cb);

/// Lock the buffer for push operation.
/// If it returns valid pointer, you can copy data there and
/// you must unlock the buffer by calling ::commBufferPushUnlock
/// You should not perform any other operations before unlocking.
/// If it returns NULL, the buffer is not locked.
CommBufferItem* commBufferPushLockTimeout(CommBuffer* cb, systime_t timeout);

/// Unlock buffer previously locked by ::commBufferPushLock, after an item has been filled.
void commBufferPushUnlock(CommBuffer* cb);

/// Like ::commBufferPushLock but used from ISR context.
CommBufferItem* commBufferPushLockI(CommBuffer* cb);

/// Like ::commBufferPushUnlock but used from ISR context.
void commBufferPushUnlockI(CommBuffer* cb);

/// Lock the buffer for pop operation.
/// If it returns valid pointer, you can copy data from there and
/// you must unlock the buffer by calling ::commBufferPopUnlock or ::commBufferUnlock.
/// You should not perform any other operations before unlocking.
/// If it returns NULL, the buffer is not locked.
CommBufferItem* commBufferPopLock(CommBuffer* cb);

/// Unlock buffer previously locked by
/// ::commBufferPopLock without modifying the buffer.
void commBufferUnlock(CommBuffer* cb);

/// Unlock buffer previously locked by ::commBufferPushLock, after an item has been read.
void commBufferPopUnlock(CommBuffer* cb);

extern CommBuffer comm_buffer;

#endif /* COMMBUFFER_H_ */
