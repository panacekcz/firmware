# Robot Firmware - 2014 Vlastimil Dort

FEATURESRC+=\
$(WORKSPACE)/Communication/communication.c\
$(WORKSPACE)/Communication/commcan.c\
$(WORKSPACE)/Communication/commusb.c\
$(WORKSPACE)/Communication/commpdo.c\
$(WORKSPACE)/Communication/commsdo.c\
$(WORKSPACE)/Communication/commnmt.c\
$(WORKSPACE)/Communication/commserial.c\
$(WORKSPACE)/Communication/commtext.c\
$(WORKSPACE)/Communication/commbuffer.c\
$(WORKSPACE)/Communication/commuart.c\
$(WORKSPACE)/Communication/commusbcfg.c

FEATUREINC+=$(WORKSPACE)/Communication