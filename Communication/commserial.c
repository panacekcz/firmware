/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "communication.h"
#include "commserial.h"
#include "commtext.h"
#include "hal.h"

#if COMM_DIAGNOSTICS || defined(__DOXYGEN__)
uint32_t comm_serial_invalid_frames;
#endif

void commSerialFrame(const CommSerial* serial){

	uint8_t sid1 = commTextFromHexDigit(serial->line->data[1]);
	uint8_t sid2 = commTextFromHexDigit(serial->line->data[2]);
	uint8_t sid3 = commTextFromHexDigit(serial->line->data[3]);

	if(sid1 == 255 || sid2 == 255 || sid3 == 255){
#if COMM_DIAGNOSTICS
		comm_serial_invalid_frames++;
#endif
		return;
	}
	comm_frame.sid  = sid1 << 8 |	sid2 << 4 |	sid3;
	comm_frame.length  = commTextFromHexDigit(serial->line->data[4]);
	if(comm_frame.length == 255){
#if COMM_DIAGNOSTICS
		comm_serial_invalid_frames++;
#endif
		return;
	}

	int i;
	for(i=0; i<comm_frame.length; ++i){
		uint8_t data1 = commTextFromHexDigit(serial->line->data[5 + 2 * i]);
		uint8_t data2 = commTextFromHexDigit(serial->line->data[6 + 2 * i]);
		if(data1 == 255 || data2 == 255){
#if COMM_DIAGNOSTICS
			comm_serial_invalid_frames++;
#endif
			return;
		}
		comm_frame.data8[i] = data1 << 4 | data2;
	}

	commDispatchFrame(serial->source);
}

__attribute__((weak))
void commSerialUserCommand(const CommSerial* serial){
	(void)serial;
}

static void commSerialEcho(const CommSerial* serial){
	BaseSequentialStream* ss = (BaseSequentialStream*) serial->channel;
	printsln(ss, "e");
}

#if COMM_DIAGNOSTICS
// This is parsed in Tools/fw/diagnostics.py
const volatile uint32_t* const debug_locations[]= {
	&comm_sdo_errors,
	&comm_pdo_missed_frames,
#if COMM_ENABLE_CAN
	&comm_can_missed_transmissions,
#else
	NULL,
#endif
#if COMM_CAN_FAST_READER
	&comm_can_missed_frames_fast,
#else
	NULL,
#endif
#if COMM_ENABLE_CAN
	&CAN1->MCR, &CAN1->MSR, &CAN1->BTR, &CAN1->TSR, &CAN1->ESR,
#else
	NULL,NULL,NULL,NULL,NULL,
#endif
};

static void commSerialDiagnostics(const CommSerial* serial){
	BaseSequentialStream* ss = (BaseSequentialStream*) serial->channel;

	for(uint32_t i = 0; i < sizeof(debug_locations) / sizeof(*debug_locations); ++i){
		prints(ss, "d");
		if(debug_locations[i] == NULL)
			prints(ss, "--------");
		else
			printx8(ss, *debug_locations[i]);
		println(ss);
	}
}
#endif

void commSerialOutputMode(const CommSerial* serial){
	uint32_t i;
	uint32_t mode = 0;
	for(i = 1; i < serial->line->length; ++i){
		switch(serial->line->data[i]){
		case 'm':
			mode |= 1 << COMM_SOURCE_MODULE;
			break;
#if COMM_ENABLE_CAN
		case 'c':
			mode |= 1 << COMM_SOURCE_CAN;
			break;
#endif
#if COMM_ENABLE_USB
		case 'u':
			mode |= 1 << COMM_SOURCE_USB;
			break;
#endif
#if COMM_ENABLE_UART1
		case 'a':
			mode |= 1 << COMM_SOURCE_UART1;
			break;
#endif
#if COMM_ENABLE_UART2
		case 'b':
			mode |= 1 << COMM_SOURCE_UART2;
			break;
#endif
		}
	}
	comm_output_mode = mode;
}

void commSerialCommand(const CommSerial* serial){
	switch(serial->line->data[0]){
	case 't':
		commSerialFrame(serial);
		break;
	case 'e':
		commSerialEcho(serial);
		break;
	case 'o':
		commSerialOutputMode(serial);
		break;
#if COMM_DIAGNOSTICS
	case 'd':
		commSerialDiagnostics(serial);
		break;
#endif
	default:
		commSerialUserCommand(serial);
		break;
	}
}


void commSerialReceiveAll(const CommSerial* serial){
	for(;;){
		int c = chnGetTimeout(serial->channel, TIME_IMMEDIATE);
		if(c == Q_RESET || c == Q_TIMEOUT)
			break;
		else if(c == '\r' || c == '\n'){
			serial->line->data[serial->line->length] = 0;
			commSerialCommand(serial);
			serial->line->length = 0;
		}
		else if(serial->line->length < COMM_SERIAL_LINE_SIZE - 1)
			serial->line->data[serial->line->length++] = c;
	}
}


void commSerialTransmit(const CommSerial* serial){
	BaseSequentialStream* ss = (BaseSequentialStream*)serial->channel;
	chSequentialStreamPut(ss, 't');
	printxn(ss, comm_frame.sid, 3);
	printx1(ss, comm_frame.length);
	int i;
	for(i=0;i<comm_frame.length;++i){
		printx2(ss, comm_frame.data8[i]);
	}
	println(ss);
}

