/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COMMUNICATION_H_
#define COMMUNICATION_H_

#include "ch.h"

#if defined(__DOXYGEN__) || !defined(COMM_MODULE_ID)
/// CAN node id.
#define COMM_MODULE_ID 0x01
#endif

#if defined(__DOXYGEN__) || !defined(COMM_DEBUG_LEDS)
/// Set to 1 to enable debugging LEDs on TestShield on TOP. Default 0 to disable.
#define COMM_DEBUG_LEDS 0
#endif

#if defined(__DOXYGEN__) || !defined(COMM_ENABLE_CAN)
/// Default 1 to enable CAN interface. Set to 0 to disable.
#define COMM_ENABLE_CAN 1
#endif

#if defined(__DOXYGEN__) || !defined(COMM_ENABLE_USB)
/// Default 1 to enable USB interface (CDC serial line). Set to 0 to disable.
#define COMM_ENABLE_USB 1
#endif

#if defined(__DOXYGEN__) || !defined(COMM_ENABLE_UART1)
/// Set to 1 to enable UART 1 interface (serial line). Default 0 to disable.
#define COMM_ENABLE_UART1 0
#endif

#if defined(__DOXYGEN__) || !defined(COMM_ENABLE_UART2)
/// Set to 1 to enable UART 2 interface (serial line). Default 0 to disable.
#define COMM_ENABLE_UART2 0
#endif

#if defined(__DOXYGEN__) || !defined(COMM_ENABLE_BUFFER)
/// Set to 0 to disable buffer. Default 1 to enable.
#define COMM_ENABLE_BUFFER TRUE
#endif

#if defined(__DOXYGEN__) || !defined(COMM_HEARTBEAT)
/// Default 1000 milliseconds heartbeat interval. Set to 0 to disable.
#define COMM_HEARTBEAT 1000
#endif


#if defined(__DOXYGEN__) || !defined(COMM_USE_CHPRINTF)
/// Default 0 to use functions in commtext.c. Set to 1 to use chprintf instead.
#define COMM_USE_CHPRINTF 0
#endif

#if defined(__DOXYGEN__) || !defined(COMM_DIAGNOSTICS)
/// Default 1 to log communication errors. Set to 0 to disable.
#define COMM_DIAGNOSTICS 1
#endif

#if defined(__DOXYGEN__) || !defined(COMM_CAN_FAST_READER)
/// Set to 1 to enable fast reader thread for CAN as a workaround for no CAN buffering.
/// Default 0 to receive CAN frames in communication thread.
#define COMM_CAN_FAST_READER 0
#endif

#if defined(__DOXYGEN__) || !defined(COMM_ENABLE_FILTERS)
/// Set to 0 to disable CAN filters (if disabled, everything is received). Default 1 to enable.
#define COMM_ENABLE_FILTERS TRUE
#endif

#if defined(__DOXYGEN__) || !defined(COMM_SDO_TIMEOUT)
/// Timeout for SDO session in milliseconds, or 0 for infinite session.
/// Default 500 ms.
#define COMM_SDO_TIMEOUT 500
#endif

enum{
	COMM_SDO_ERROR_TOGGLE    = 0x05030000,
	COMM_SDO_ERROR_TIMEOUT   = 0x05040000,
	COMM_SDO_ERROR_WRITEONLY = 0x06010001,
	COMM_SDO_ERROR_READONLY  = 0x06010002,
	COMM_SDO_ERROR_NOTEXIST  = 0x06020000,
	COMM_SDO_ERROR_SUBINDEX = 0x06090011,
	COMM_SDO_ERROR_TYPE  = 0x06070010,
	COMM_SDO_ERROR_LENGTH_HIGH  = 0x06070012,
	COMM_SDO_ERROR_LENGTH_LOW  = 0x06070013,

	COMM_SDO_ERROR_RANGE     = 0x06090030,

	COMM_SDO_ERROR_DATA_STORE   = 0x08000020,
};

typedef enum{
	COMM_NMT_RESET = 0x0000,
	COMM_NMT_BOOTUP = 0x0100,
	COMM_NMT_STOPPED = 0x0104,
	COMM_NMT_OPERATIONAL = 0x0105,
	COMM_NMT_PREOPERATIONAL = 0x017F,
} CommNmtState;

typedef enum{
	/// SDO write started.
	/// comm_sdo.size contains the size.
	/// comm_sdo.error contains 0
	/// Store error code to comm_sdo.error on error.
	COMM_SDO_WRITE_START,
	/// SDO data available.
	/// comm_sdo.size contains total size.
	/// comm_sdo.segment contains number of available bytes
	/// comm_sdo_data8,16,32 contain the available data
	/// comm_sdo.index is index of data within the object
	/// comm_sdo.error contains 0.
	/// Store error code to comm_sdo.error on error.
	COMM_SDO_WRITE_DATA,
	/// SDO all data written.
	/// comm_sdo.size contains total size.
	/// comm_sdo.error contains 0.
	COMM_SDO_WRITE_END,
	/// SDO read start.
	/// Write the size to ::comm_sdo.size.
	/// comm_sdo.error contains 0.
	/// Store error code to ::comm_sdo.error on error.
	COMM_SDO_READ_START,
	/// SDO read data.
	/// comm_sdo.size contains total size.
	/// comm_sdo.segment contains number of needed bytes
	/// Store the data to comm_sdo.data8, 16 or 32
	/// comm_sdo.index is index of data within the object
	/// comm_sdo.error contains 0.
	/// Store error code to ::comm_sdo.error on error.
	COMM_SDO_READ_DATA,
	/// SDO all data read.
	/// comm_sdo.size contains total size.
	/// comm_sdo.error contains 0.
	COMM_SDO_READ_END,
	/// SDO write premature end.
	/// comm_sdo.size contains total size.
	/// comm_sdo.index contains number of successfully written bytes.
	/// comm_sdo.error contains the error code.
	/// Store error code to comm_sdo.error to override the error code.
	COMM_SDO_WRITE_BREAK,
	/// SDO read premature end.
	/// comm_sdo.size contains total size.
	/// comm_sdo.index contains number of successfully read bytes.
	/// comm_sdo.error contains the error code.
	/// Store error code to comm_sdo.error to override the error code.
	COMM_SDO_READ_BREAK,

	/// PDO data available.
	/// Read the data directly from commFrame.
	COMM_PDO,
} CommHandlerEvent;

typedef enum{
	COMM_SOURCE_NONE, ///< Unknown or invalid frame source.
	COMM_SOURCE_MODULE, ///< Frame originated by application.
	COMM_SOURCE_CAN, ///< Frame received from CAN.
	COMM_SOURCE_USB, ///< Frame received from USB serial.
	COMM_SOURCE_UART1, ///< Frame received from UART serial 1.
	COMM_SOURCE_UART2, ///< Frame received from UART serial 2.
} CommFrameSource;

typedef struct{
	/// Source identifier
	CommFrameSource id;
	/// Initialization procedure. Can be ::NULL.
	void (*init)(void);
	/// Receive all procedure.  Can be ::NULL.
	void (*receive)(void);
	/// Transmit frame procedure.  Can be ::NULL.
	void (*transmit)(void);
	/// Wait until it is safe to stop.  Can be ::NULL.
	void (*fini)(void);
	/// Driver event (signaled on every frame or more often). Can be ::NULL.
	EventSource* event;
} CommSourceDriver;

/// Communication frame.
/// This structure is a common structure for all sources.
typedef struct{
	union {
		uint8_t data8[8];
		uint16_t data16[4];
		uint32_t data32[2];
		float dataF[2];
	};
	/// Standard identifier of the message.
	uint16_t sid;
	/// Length of the data (between 0 and 8)
	uint8_t length;
} CommFrame;

typedef void (*CommHandler)(CommHandlerEvent evt);

/// Handler specification etry pro SDO or PDO.
typedef struct {
	/// Identifier of the PDO or SDO. In case of PDO, it is sid-0x180.
	uint16_t id;
	/// The handler function.
	CommHandler handler;
	/// Parameter to the entry.
	uint32_t param;
} CommHandlerEntry;

/// Initialize communication and start communication thread.
/// The thread will perform all actions.
Thread* commStart(void);
/// Initialize communication and start all threads related to communication.
void commStartAll(void);
/// Dispatch commFrame.
void commDispatchFrame(CommFrameSource source);

msg_t commPdoSend(const CommFrame* fr, systime_t timeout);
msg_t commPdoSendI(const CommFrame* fr);

typedef struct{
	union {
		uint8_t data8[7];
		uint16_t data16[3];
		uint32_t data32;
	};
	uint32_t index;
	uint32_t size;
	uint32_t segment;
	uint32_t error;
	uint16_t object_index;
	uint8_t object_subindex;
} CommSdo;

extern uint32_t comm_output_mode;
extern CommFrame comm_frame;
extern CommSdo comm_sdo;

extern const CommHandlerEntry* comm_sdo_handler;

extern const CommHandlerEntry comm_sdo_handlers[], comm_pdo_handlers[];
extern const uint32_t comm_sdo_handler_count, comm_pdo_handler_count;

extern const CommSourceDriver comm_drivers[];
extern const uint32_t comm_driver_count;

#if defined(__DOXYGEN__) || COMM_DIAGNOSTICS
/// Number of PDOs which were not sent due to full buffer. Can be modified outside communication thread.
extern uint32_t comm_pdo_missed_frames;
/// Number of SDO errors (timeouts, invalid frames, user errors, ...)
extern uint32_t comm_sdo_errors;
/// Number of CAN frames that were not sent due to full output queue and timeout.
extern uint32_t comm_can_missed_transmissions;
#if defined(__DOXYGEN__) || COMM_CAN_FAST_READER
/// Number of CAN frames that were not received due to full buffer. Can be modified outside communication thread.
extern uint32_t comm_can_missed_frames_fast;
#endif
#endif

#if defined(__DOXYGEN__) || COMM_ENABLE_USB
/// Check whether the USB serial is connected and active.
bool_t commUsbIsConnected(void);
#endif

#if defined(__DOXYGEN__) || COMM_CAN_FAST_READER
/// Start fast reader thread for CAN.
Thread* commCanStartFastReader(void);
#endif

/// This function is used instead of memcpy because it is smaller
void commCopy(uint8_t* d, const uint8_t* s, int l);

/// This function should be implemented by users to handle
/// communication termination.
extern void commTerminated(void);
extern bool_t commNmtStateChanging(uint16_t old_state, uint16_t new_state);
void commNmtChangeState(uint16_t new_state);
uint16_t commNmtState(void);
uint16_t commNmtStateI(void);

void commEnableOutputHandler(CommHandlerEvent e);

#endif /* COMMUNICATION_H_ */
