/*
    ChibiOS/RT - Copyright (C) 2006-2013 Giovanni Di Sirio
    Robot Firmware - Modified 2014 by Vlastimil Dort

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include "commusb.h"
#include "communication.h"

#if COMM_ENABLE_USB

/// USB Device Descriptor.
const uint8_t vcom_device_descriptor_data[18] = {
	USB_DESC_DEVICE(
		0x0110, // bcdUSB (1.1).
		0x02, // bDeviceClass (CDC).
		0x00, // bDeviceSubClass.
		0x00, // bDeviceProtocol.
		0x40, // bMaxPacketSize.
		0x0483, // idVendor (ST).
		0x5740, // idProduct.
		0x0200, // bcdDevice.
		0, // iManufacturer.
		0, // iProduct.
		0, // iSerialNumber.
		1// bNumConfigurations.
	)
};

/// Configuration Descriptor tree for a CDC.
const uint8_t vcom_configuration_descriptor_data[67] = {
	USB_DESC_CONFIGURATION(// Configuration Descriptor.
		67, // wTotalLength.
		0x02, // bNumInterfaces.
		0x01, // bConfigurationValue.
		0, // iConfiguration.
		0x80, // bmAttributes (self powered).
		250 // bMaxPower (100mA).
	),
	USB_DESC_INTERFACE(// Interface Descriptor.
		0x00, // bInterfaceNumber.
		0x00, // bAlternateSetting.
		0x01, // bNumEndpoints.
		0x02, // bInterfaceClass (Communications Interface Class, CDC section 4.2).
		0x02, // bInterfaceSubClass (Abstract Control Model, CDC section 4.3).
		0x01, // bInterfaceProtocol (AT commands, CDC section 4.4).
		0 // iInterface.
	),
	// Header Functional Descriptor (CDC section 5.2.3).
	USB_DESC_BYTE(5), // bLength.
	USB_DESC_BYTE(0x24), // bDescriptorType (CS_INTERFACE).
	USB_DESC_BYTE(0x00), // bDescriptorSubtype (Header Functional Descriptor.
	USB_DESC_BCD(0x0110), // bcdCDC.
	// Call Management Functional Descriptor.
	USB_DESC_BYTE(5), // bFunctionLength.
	USB_DESC_BYTE(0x24), // bDescriptorType (CS_INTERFACE).
	USB_DESC_BYTE(0x01), // bDescriptorSubtype (Call Management Functional Descriptor).
	USB_DESC_BYTE(0x00), // bmCapabilities (D0+D1).
	USB_DESC_BYTE(0x01), // bDataInterface.
	// ACM Functional Descriptor.
	USB_DESC_BYTE(4), // bFunctionLength.
	USB_DESC_BYTE(0x24), // bDescriptorType (CS_INTERFACE).
	USB_DESC_BYTE(0x02), // bDescriptorSubtype (Abstract Control Management Descriptor).
	USB_DESC_BYTE(0x02), // bmCapabilities.
	// Union Functional Descriptor.
	USB_DESC_BYTE(5), // bFunctionLength.
	USB_DESC_BYTE(0x24), // bDescriptorType (CS_INTERFACE).
	USB_DESC_BYTE(0x06), // bDescriptorSubtype (Union Functional Descriptor).
	USB_DESC_BYTE(0x00), // bMasterInterface (Communication Class Interface).
	USB_DESC_BYTE(0x01), // bSlaveInterface0 (Data Class Interface).
	USB_DESC_ENDPOINT( // Endpoint 2 Descriptor.
		USBD1_INTERRUPT_REQUEST_EP | 0x80, 0x03, // bmAttributes (Interrupt).
		0x0008, // wMaxPacketSize.
		0xFF // bInterval.
	),
	USB_DESC_INTERFACE( // Interface Descriptor.
		0x01, // bInterfaceNumber.
		0x00, // bAlternateSetting.
		0x02, // bNumEndpoints.
		0x0A, // bInterfaceClass (Data Class Interface, CDC section 4.5).
		0x00, // bInterfaceSubClass (CDC section 4.6).
		0x00, // bInterfaceProtocol (CDC section 4.7).
		0x00 // iInterface.
	),
	USB_DESC_ENDPOINT( // Endpoint 3 Descriptor.
		USBD1_DATA_AVAILABLE_EP, // bEndpointAddress.
		0x02, // bmAttributes (Bulk).
		0x0040, // wMaxPacketSize.
		0x00 // bInterval.
	),
	USB_DESC_ENDPOINT( // Endpoint 1 Descriptor.
		USBD1_DATA_REQUEST_EP | 0x80, // bEndpointAddress.
		0x02, // bmAttributes (Bulk).
		0x0040, // wMaxPacketSize.
		0x00  // bInterval.
	)
};

/// U.S. English language identifier.
const uint8_t vcom_string0[4] = {
	USB_DESC_BYTE(4), // bLength.
	USB_DESC_BYTE(USB_DESCRIPTOR_STRING), // bDescriptorType.
	USB_DESC_WORD(0x0409) // wLANGID (U.S. English).
};

#endif
