# Robot Firmware - 2014 Vlastimil Dort

# Select the directory in Module which will be used for this configuration
MODULE_CONF=ab2f107

USE_DEBUG?=no
USE_LOADER?=yes


# In this configuration, TestShieldTest is also used. 
include $(WORKSPACE)/TestShieldTest/feature.mk

# Here is an example how to trigger some actions when configuration 
# is selected or cleaned. This can be used to generate source code
# by Python depending on the configuration.

config: sampleconfig
distclean: sampledistclean

sampledistclean:
	@echo "Sample distclean goal"
sampleconfig:
	@echo "Sample configuration goal with $(CONF_BUILD) $(CONF_MODULEID)"

.PHONY: sampleconfig sampledistclean