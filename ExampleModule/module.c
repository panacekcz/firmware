/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "module.h"
#include "example.h"
#include "armboard.h"

void moduleInit(void){
	// moduleInit is called before communication is initialized
	exampleInit();
}
void moduleMain(void){
	// moduleMain is called after communication is initialized
	exampleStartThread();

	// moduleMain should not return, but it should do some work.
	// You can create an event loop and call feature functions.
	for(;;){
		palTogglePad(GPIOC, GPIOC_LED14);
		chThdSleepMilliseconds(100);
	}
}

bool_t commNmtStateChanging(uint16_t o, uint16_t n){
	(void)o;
	(void)n;

	if(o == COMM_NMT_PREOPERATIONAL && n == COMM_NMT_OPERATIONAL){
		// Here you can react to the transition from pre-operational to operational state
		// You can for example use a semaphore to start/stop features which should run only
		// in operational state.

		// ExampleFeature handles it itself by polling commNmtState before sending PDOs, which is
		// usually sufficent, so no handling is required here

		// Setting the led just for illustration
		palSetPad(GPIOC, GPIOC_LED13);
	}
	else if(o == COMM_NMT_OPERATIONAL && n == COMM_NMT_PREOPERATIONAL){
		palClearPad(GPIOC, GPIOC_LED13);
	}

	return TRUE;
}
