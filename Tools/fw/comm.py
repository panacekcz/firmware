#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.

import serial
import time
import binascii
import re
import sys
import threading
from serial.serialutil import SerialException
import fw.config as config

# Factory functions

def createSerialComm(serial):
    return SerialStrictComm(serial) if config.getStrictComm() else SerialFilteringComm(serial)
def createCanComm(can):
    return CanStrictComm(can) if config.getStrictComm() else CanFilteringComm(can)

def createPrimarySerial(timeout):
    '''Create serial communication using current configuration'''
    return createSerialComm(Serial(config.getPrimarySerial(), timeout))

def createSecondarySerial(timeout):
    '''Create secondary serial communication using current configuration'''
    return createSerialComm(Serial(config.getSecondarySerial(), timeout))

def createLoaderSerial(timeout):
    return createSerialComm(Serial(config.getLoaderSerial(), timeout))

def createCan(timeout):
    '''Create can communication using current configuration'''
    can_name = config.getCan()
    if can_name == "slcan":
        return createCanComm(Slcan(createPrimarySerial(timeout)))

# Exceptions
class SerialCommException(Exception):
    pass
class SlcanException(Exception):
    pass
class DisconnectedException(Exception):
    pass
# Classes

class Timeout(threading.Thread):
    # define state constants
    Stopped, Started, Waiting, Elapsed, Ended = range(5)
    def __init__(self, time):
        threading.Thread.__init__(self);
        self.time = time
        self.cond = threading.Condition()
        self.state = self.Stopped
        self.level = 0
        self.start()
        
    def elapsed(self):
        with self.cond:
            return self.state == self.Elapsed
    def run(self):
        with self.cond:
            while True:
                time = None
                if self.state == self.Ended:
                    return
                elif self.state == self.Waiting:
                    self.state = self.Elapsed
                elif self.state == self.Started:
                    self.state = self.Waiting
                    time = self.time
                self.cond.wait(time)
    def __enter__(self):
        if self.level == 0:
            with self.cond:
                self.state = self.Started
                self.cond.notify()
        self.level += 1
        return self
    
    def __exit__(self,_type,_value,_traceback):
        self.level -= 1
        if self.level == 0:
            with self.cond:
                self.state = self.Stopped
                self.cond.notify()
    def close(self):
        with self.cond:
            self.state = self.Ended
            self.cond.notify()
        self.join()

class Serial(object):
    '''Writing to and reading from a serial line'''
    
    serial = None
    verbose = config.getSerialVerbose()
    
    def write(self, data):
        '''
        Write data to serial port.
        If verbose serial is enabled, it is also printed to standard output.
        Args:
            data: A binary string.
        Raises:
            DisconnectedException: When the data cannot be written (assuming device disconnected).
        '''
        try:
            if self.verbose:
                print 'Write to serial:', repr(data)
            self.serial.write(data)
        except SerialException as e:
            # Every exception is interpreted as disconnected
            raise DisconnectedException(str(e)), None, sys.exc_info()[2]
        
    def readTerminator(self, terminator):
        '''
        Read until and including the specified terminator string.
        If verbose serial is enabled, it is also printed to standard output.
        If the terminator is not encountered within the specified timeout, None or a part of a line may be returned.
        Args:
            terminator: A binary string indicating the end.
            timeout: The timeout in seconds.
        Returns:
            A binary string or None.
        Raises:
            DisconnectedException: When the data cannot be written (assuming device disconnected).
        '''
        try:
            r = ''
            self.serial.timeout = self.timeout.time
            with self.timeout:
                while not r.endswith(terminator) and not self.timeout.elapsed():
                    nextchar = self.serial.read(1)
                    if nextchar:
                        r+=nextchar
                    else:
                        break
            if self.verbose:
                print 'Read from serial:', repr(r)
            return r
        except SerialException as e:
            # Every exception is interpreted as disconnected
            raise DisconnectedException(str(e)), None, sys.exc_info()[2]
    
    def close(self):
        '''Close the serial line'''
        self.serial.close()

    def __init__(self, serialName, timeout):
        '''Open the serial line (trying multiple times)'''
        self.timeout = timeout
        count = 0
        maxcount = config.getSerialRetryCount()
        interval = config.getSerialRetryInterval()
        while self.serial is None:
            try:
                self.serial = serial.Serial(serialName)
            except SerialException as e:
                time.sleep(interval)
                count += 1
                if count > maxcount:
                    raise e
                    
    def __enter__(self):
        return self
    def __exit__(self,_type,_value,_traceback):
        self.close()

class SerialComm:
    
    serial = None
    terminator = '\r'
    
    def writeCommand(self, command, content = ''):
        '''
        Write a command line to a serial.
        Args:
            command: Command character.
            content: Content string.
        '''
        self.serial.write(command + content + self.terminator)
    
    def readAnyCommand(self):
        '''
        Read the first available command from a serial.
        Returns:
            command: Command character
            content: Command content
        Raises:
            DisconnectedException: When the serial disconnected
            SerialCommException: When the read data is not a valid command line
        '''
        line = self.serial.readTerminator(self.terminator)
        if line is None or line is '':
            return None
        if len(line) < 2 or not line.endswith(self.terminator):
            if self.timeout.elapsed:
                return None
            else:
                raise SerialCommException("Invalid serial line", line)
        command = line[0]
        content = line[1:-1]
        return command, content
    
    def expectDisconnect(self, test):
        test.assertTrue(self.readDisconnect())
        
    def close(self):
        self.serial.close()
    def __init__(self, serial):
        self.serial = serial
        self.timeout = serial.timeout
    def __enter__(self):
        return self
    def __exit__(self,_type,_value,_traceback):
        self.close()


class SerialFilteringComm(SerialComm):
    '''Serial communicators which ignores unexpected commands'''
    
    def readCommand(self, accept):
        '''
        Read command from a specified set. Ignore commands not in the set
        Raises:
            DisconnectedException
            SerialCommException
        '''
        with self.timeout:
            while not self.timeout.elapsed():
                line = self.readAnyCommand()
                if line is None:
                    return None
                if line[0] in accept:
                    return line
        return None
    
    def readDisconnect(self):
        '''Wait until the serial disconnects or the timeout elapses'''
        try:
            with self.timeout:
                while not self.timeout.elapsed():
                    self.readAnyCommand()
            return not config.getDisconnectOnReset()
        except DisconnectedException:
            return True

    def expectNothing(self, test):
        pass
    def expectCommand(self, test, command, content = ''):
        line = self.readCommand([command])
        test.assertIsNotNone(line)
        test.assertEqual(line[1], content)
    def expectCommandPattern(self, test, command, pattern = ''):
        line = self.readCommand([command])
        test.assertIsNotNone(line)
        test.assertRegexpMatches(line[1], '^' + pattern + '$')


class SerialStrictComm(SerialComm):
    '''Serial communicator'''
    
    def readDisconnect(self):
        '''Wait until the serial disconnects or the timeout elapses'''
        try:
            if self.readAnyCommand() is not None:
                return False
            return not config.getDisconnectOnReset()
        except DisconnectedException:
            return True
        
    def readCommand(self, accept):
        return self.readAnyCommand()
    
    def expectNothing(self, test):
        '''Assert there is no input on the serial line'''
        test.assertIsNone(self.readAnyCommand())
        
    def expectCommand(self, test, command, content = ''):
        '''Read a command line and assert it to be equal the specified command line'''
        test.assertEqual(self.serial.readTerminator(self.terminator), command + content + '\r')
    def expectCommandPattern(self, test, command, pattern):
        '''Read a command line and assert it to match a pattern'''
        test.assertRegexpMatches(self.serial.readTerminator(self.terminator), '^' + command + pattern + '\\r$')

class Slcan(object):
    '''Using and testing CAN over serial ASCII'''
    serial = None
    
    def writeFrame(self, address, data):
        '''Write CAN frame to serial line'''
        if len(data) > 8:
            raise SlcanException('Frame too long')
        line = "{0:03x}{1:01x}{2:s}".format(address, len(data), binascii.b2a_hex(data))
        self.serial.writeCommand('t', line)
        
    def readFrame(self):
        '''Read a line from the serial line and interpret it as a CAN frame'''
        line = self.serial.readCommand('t')
        if line is None:
            return None
        command, content = line
        if command is not 't':
            raise SlcanException('Wrong SLCAN response command', command + content)
        match = re.match(r"(?P<addr>[0-9a-fA-F]{3})(?P<length>[0-9a-fA-F])(?P<data>(?:[0-9a-fA-F]{2}){0,8})", content)
        if match is None:
            raise SlcanException('Wrong SLCAN line format', command + content)
        address = int(match.group('addr'), 16)
        length = int(match.group('length'), 16)
        data = binascii.a2b_hex(match.group('data'))
        if length != len(data):
            raise SlcanException('Wrong SLCAN length', command + content)
        return address, data

    def close(self):
        '''Close the serial line'''
        self.serial.close()
        
    def __init__(self, serial):
        self.serial = serial
        self.timeout = serial.timeout
        self.serial.serial.write(config.getSlcanInit())
    def __enter__(self):
        return self
    def __exit__(self, _type, _value, _traceback):
        self.close()

class CanComm(object):
        
    def writeFrame(self, address, data):
        return self.can.writeFrame(address, data)
    def readAnyFrame(self):
        return self.can.readFrame()
    def expectDisconnect(self, test):
        test.assertTrue(self.readDisconnect())

    def expectFrame(self, test, address, data):
        '''Read a frame from the serial and assert it to be equal to the specified frame'''
        frame = self.readFrame([address])
        test.assertIsNotNone(frame)
        frame_address, frame_data = frame
        test.assertEqual(frame_address, address)
        test.assertEqual(frame_data, data)

    def close(self):
        '''Close the CAN'''
        self.can.close()
    def __init__(self, can):
        self.can = can
        self.timeout = can.timeout
    def __enter__(self):
        return self
    def __exit__(self, _type, _value, _traceback):
        self.close()

class CanStrictComm(CanComm):
    def readFrame(self, _accept_id):
        return self.readAnyFrame()
    def readDisconnect(self):
        try:
            if self.readAnyFrame() is not None:
                return False
            return not config.getDisconnectOnReset()
        except DisconnectedException:
            return True
    def expectNothing(self, test):
        test.assertIsNone(self.readAnyFrame())

class CanFilteringComm(CanComm):
    def readFrame(self, accept_id):
        with self.timeout:
            while not self.timeout.elapsed():
                frame = self.can.readFrame()
                if frame is None:
                    return None
                if frame[0] in accept_id:
                    return frame
        return None
    
    def readDisconnect(self):
        try:
            with self.timeout:
                while not self.timeout.elapsed():
                    self.readAnyFrame()
            return not config.getDisconnectOnReset()
        except DisconnectedException:
            return True
        
    def expectNothing(self, test):
        pass
