#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.


import subprocess
import os
import re
import fw.config as config
import fw.comm as comm
from binascii import a2b_hex
from binascii import b2a_hex
import struct
from fw.protocol import Canopen, CanopenSdo, CanopenException, CanopenNmtMaster

# Factories
def createFirmwareCompiler():
    '''Create compiler object according to configuration'''
    name = config.getCompiler()
    if name is "skip" or name is None:
        return SkipFirmwareCompiler()
    elif name is "make":
        return MakeFirmwareCompiler()

def createFirmwareUploader():
    '''Create uploader object according to configuration'''
    name = config.getUploader()
    if name is "skip" or name is None:
        return SkipFirmwareUploader()
    elif name is "openocd":
        return OpenocdFirmwareUploader()
    elif name is "ihex":
        return IhexFirmwareUploader()
    elif name is "slcan":
        return CanopenFirmwareUploader()
# Exceptions
class CompileException(Exception):
    pass
class UploadException(Exception):
    pass
class IhexException(Exception):
    pass
# Classes
class SkipFirmwareCompiler(object):
    '''Compiler object which does not compile anything'''
    def compile(self, project, variables):
        print('Compilation of {0:s} skipped'.format(project))
    def clean(self, project):
        print('Cleaning {0:s} skipped'.format(project))
    def close(self):
        pass
class SkipFirmwareUploader(object):
    '''Uploader object which does not upload anything'''
    def upload(self, project, _node_id=None, _loader_node_id=None):
        print('Upload of {0:s} skipped'.format(project))
    def setMode(self, mode):
        pass
    def close(self):
        pass


class MakeFirmwareCompiler(object):
    '''Compile firmware using GNU Make'''
    def compile(self, project, variables):
        newenv = os.environ.copy()
        if variables is not None:
            newenv.update(variables)
        directory = os.path.join(config.getWorkspace(),project)
        if subprocess.call(['make','config'], env=newenv, cwd=directory) != 0:
            raise CompileException("Make config error")
        if subprocess.call(['make','all'], env=newenv, cwd=directory) != 0:
            raise CompileException("Make returned error")
        
    def clean(self, project):
        directory = os.path.join(config.getWorkspace(),project)
        if subprocess.call(['make','distclean'], cwd=directory) != 0:
            raise CompileException("Make distclean error")
    def close(self):
        pass
class OpenocdFirmwareUploader(object):
    '''Upload firmware using OpenOCD and GDB'''
    
    modes = {
        'app':('Module','upload.script'),
        'debug':('Module', 'upload-debug.script'),
        'loader':('Loader', 'upload.script'),
    }
    upload_script_dir = 'Module'
    upload_script_name = 'upload.script'
    
    def upload(self, project, _node_id=None, _loader_node_id=None):
        bconf = config.getBuildConfiguration()
        cfgfile = os.path.join('..','Module',bconf,'openocd.cfg')
        connectfile = os.path.join('..','Module','connect.script')
        uploadfile = os.path.join('..', self.upload_script_dir, bconf, self.upload_script_name)
        runfile = os.path.join('..',self.upload_script_dir,bconf,'run.script')
        directory = os.path.join(config.getWorkspace(),project)
        
        if 'OPENOCD' in os.environ:
            openocd_binary = os.path.join(os.environ['OPENOCD'], 'openocd')
        else:
            openocd_binary = 'openocd'
        openocd = subprocess.Popen([openocd_binary,'-f', cfgfile], cwd = directory)
        try:
            subprocess.call(['gdb','-batch', '-x',connectfile,'-x',uploadfile,'-x',runfile], cwd = directory)
        finally:
            openocd.kill()
    def setMode(self, mode):
        if mode in self.modes:
            self.upload_script_dir = self.modes[mode][0]
            self.upload_script_name = self.modes[mode][1]
        else:
            raise UploadException('Invalid mode')
    def close(self):
        pass
class IhexFirmwareUploader(object):
    '''Upload firmware by dumping the ihex file to serial'''
    address_command = 'A'
    reset_command = 'a'
    
    def __init__(self):
        self.timeout = comm.Timeout(1)
    def close(self):
        self.timeout.close()
    
    def setMode(self, mode):
        if mode == "app":
            self.address_command = 'A'
            self.reset_command = 'a'
        elif mode == "loader":
            self.address_command = 'L'
            self.reset_command = 'l'
        else:
            raise UploadException('Invalid mode')
    
    def upload(self, project, _node_id=None, _loader_node_id=None):
        hexpath = os.path.join(config.getWorkspace(),project,"build","ch.hex")
        with comm.createLoaderSerial(self.timeout) as serial:
            serial.writeCommand('l','l')
            if not serial.readDisconnect():
                raise UploadException('Device did not reset into loader')
        with comm.createLoaderSerial(self.timeout) as serial:
            # Read and check test string
            serial.writeCommand('l', 't')
            line = serial.readCommand('a')
            if line is None:
                raise UploadException('No response test string')
            command, content = line
            if command is not 'a':
                raise UploadException('Wrong response test string')
            if not re.match('LOA[a-f0-9]{2}', content):
                raise UploadException('Invalid test string from loader')
            # Set address range
            serial.writeCommand('l', self.address_command)
            # Upload whole hex file
            with open(hexpath, 'r') as hexfile:
                for line in hexfile:
                    serial.serial.write(line) # The line already contains line terminator
            # Expect end confirm
            confirm = serial.readCommand(['a', 'x'])
            if confirm is None:
                raise UploadException('No response to EOF command')
            command, content = confirm
            if command != 'a':
                raise UploadException('Error during load ' + command + content)
            if content != 'HXE':
                raise UploadException('Invalid confirm response ' + command + content)
            # Reset to application
            serial.writeCommand('l', self.reset_command)
            if not serial.readDisconnect():
                raise UploadException('Loader did not reset after write')

class CanopenUploaderClient(object):
    
    data_object = 0x3E02
    addr_object = 0x3E03
    command_object = 0x3E04
    sdo = None
    
    def setCurrentAddress(self, address):
        self.sdo.writeObject(self.node_id, self.addr_object, 1, struct.pack("<I",address))
    
    def setLowAddress(self, address):
        self.sdo.writeObject(self.node_id, self.addr_object, 2, struct.pack("<I",address))
        
    def setHighAddress(self, address):
        self.sdo.writeObject(self.node_id, self.addr_object, 3, struct.pack("<I",address))
    
    def setAddresses(self, current, low, high):
        self.sdo.writeObject(self.node_id, self.addr_object, 0, struct.pack("<III",current, low, high))
    
    def getCurrentAddress(self):
        return struct.unpack("<I", self.sdo.readObject(self.node_id, self.addr_object, 1))[0]
    
    def getLowAddress(self):
        return struct.unpack("<I", self.sdo.readObject(self.node_id, self.addr_object, 2))[0]
                             
    def getHighAddress(self):
        return struct.unpack("<I", self.sdo.readObject(self.node_id, self.addr_object, 3))[0]
    
    def getAddresses(self):
        return struct.unpack("<III", self.sdo.readObject(self.node_id, self.addr_object, 0))
    
    def endOperation(self):
        self.command(1)
    def command(self, command):
        self.sdo.writeObject(self.node_id, self.command_object, 0, struct.pack("<I",command))
    
    def downloadData(self, address, size):
        self.setLowAddress(address)
        self.setHighAddress(address + size)
        return self.sdo.readObject(self.node_id, self.data_object, 0x00)
    
    def uploadData(self, address, data):
        self.setCurrentAddress(address)
        self.sdo.writeObject(self.node_id, self.data_object, 0x00, data)

    def __init__(self, can, node_id):
        self.sdo = CanopenSdo(Canopen(can))
        self.node_id = node_id

class CanopenFirmwareUploader(object):
    '''Upload firmware by Canopen SDO objects'''
    
    class DataSegment(object):
        def __init__(self, addr, data):
            self.begin_address = addr
            self.end_address = addr + len(data)
            self.data_parts = [data]
        def addPart(self, data):
            self.data_parts.append(data)
            self.end_address+=len(data)
        def getData(self):
            return ''.join(self.data_parts)
    
    range_command = 0x11
    reset_command = 0x0101
    
    def __init__(self):
        self.timeout = comm.Timeout(1)
    def close(self):
        self.timeout.close()
    
    def _clear(self):
        self.client = None
        self.data_segments = []
        self.current_segment = None
        self.address_high = None
    
    def setMode(self, mode):
        if mode == "app":
            self.range_command = 0x11
            self.reset_command = 0x0101
        elif mode == "loader":
            self.range_command = 0x12
            self.reset_command = 0x0102
        else:
            raise UploadException("Invalid mode")
    
    def upload(self, project, node_id = None, loader_node_id = None):
        self._clear()
        hexpath = os.path.join(config.getWorkspace(),project,"build","ch.hex")
        if loader_node_id is None:
            loader_node_id = config.getLoaderNodeId()
        if node_id is None:
            node_id = config.getNodeId()
        # Reset into loader
        try:
            self.timeout.time = 1
            with comm.createCan(self.timeout) as can:
                nmt = CanopenNmtMaster(can)
                nmt.resetToLoader(node_id)
                self.timeout.time = 3
                if not can.readDisconnect():
                    raise UploadException('Device did not reset into loader')
        except CanopenException:
            print "Cannot reset from application mode, assuming loader mode"
        # Load
        self.timeout.time = 1
        with comm.createCan(self.timeout) as can:
            nmt = CanopenNmtMaster(can)
            # Check that the device is a loader
            if (nmt.getResetReason(loader_node_id) & 0xFF) != 0x02:
                raise UploadException("Device is not in loader mode")
            # Read the file, upload and verify
            self.client = CanopenUploaderClient(can, loader_node_id)
            self.client.command(self.range_command)
            parseIhex(hexpath, self)
            self.client = None
            # Reset into application
            nmt.reset(loader_node_id, self.reset_command)
            self.timeout.time = 3
            if not can.readDisconnect():
                raise UploadException('Device did not reset after load')
        self._clear()
    def ihexFileBegin(self):
        pass
    def ihexFileEnd(self):
        pass
    def ihexAddress(self, address_high):
        self.address_high = address_high
    def ihexData(self, address_low, data):
        address = (self.address_high << 16) | address_low
        if self.current_segment is not None and self.current_segment.end_address == address:
            self.current_segment.addPart(data)
        else:
            if self.current_segment is not None:
                self.data_segments.append(self.current_segment)
            self.current_segment = self.DataSegment(address, data)
        pass
    def ihexEntry(self, address):
        pass
    def ihexEof(self):
        if self.current_segment is not None:
            self.data_segments.append(self.current_segment)
        for segment in self.data_segments:
            self.client.uploadData(segment.begin_address, segment.getData())
        for segment in self.data_segments:
            verify_data = self.client.downloadData(segment.begin_address, segment.end_address-segment.begin_address)
            if verify_data != segment.getData():
                print b2a_hex(verify_data)
                print b2a_hex(segment.getData())
                raise UploadException("Verification failed")
        self.client.endOperation()
        
def parseIhex(filename, handler):
    '''Parse Ihex file and invoke callback for each command'''
    data_cmd = 0x00
    eof_cmd = 0x01
    addr_cmd = 0x04
    entry_cmd = 0x05
    
    with open(filename) as f:
        for line in f:
            match = re.match(r':((?:[a-fA-F0-9]{2}){5,})\r?\n', line)
            if match is None:
                raise IhexException('Invalid Ihex line')
            
            linedata = a2b_hex(match.group(1))
            data = linedata[4:-1]
            linelen = ord(linedata[0])
            
            if linelen != len(data):
                raise IhexException('Wrong Ihex length (specified %d, data %d)'%(linelen, len(data)))
            
            cmd = ord(linedata[3])
            offset = struct.unpack('>H',linedata[1:3])[0]
            
            if cmd == addr_cmd:
                if linelen != 2:
                    raise IhexException('Invalid address command length')
                handler.ihexAddress(struct.unpack('>H',data)[0])
            elif cmd == data_cmd:
                handler.ihexData(offset, data)
            elif cmd == eof_cmd:
                if linelen != 0:
                    raise IhexException('Invalid eof command length')
                handler.ihexEof()
            elif cmd == entry_cmd:
                if linelen != 4:
                    raise IhexException('Invalid start command length')
                handler.ihexEntry(struct.unpack('>I',data)[0])
            else:
                raise IhexException('Unknown Ihex command %#x'%cmd)