#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.

from fw.object import Module, Robot, ObjectId

modules += [
    Module('test1', 'TestModule', 'ab2f107', 0x71, 0x01),
    Module('test2', 'TestModule', 'ab2f107', 0x72, 0x02),
    Module('driver_dev', 'MotorModule', 'develop_driver', 0x71, 0x03),
    Module('driver_demo', 'MotorModule', 'demo_driver', 0x71, 0x03),
    Module('encoder_dev', 'MotorModule', 'develop_encoder', 0x71, 0x03),
    Module('motors_dev', 'MotorModule', 'develop_motor', 0x71, 0x03),
    Module('ir_dev', 'IRModule', 'develop', 0x71, 0x07),
    Module('servo_dev', 'IRModule', 'develop_servo', 0x71, 0x07),
    Module('sharp_dev', 'IRModule', 'develop_sharp', 0x71, 0x07),
    
    Robot('test', ['test1', 'test2']),
]
pdo_id+=[
    ObjectId('position','default', 0x181), # Position computed from encoders
    ObjectId('angle','default', 0x182), # Angle computed from encoders
    ObjectId('encoders','default', 0x183), # Absolute values from encoders
    ObjectId('velocity','set', 0x184), # Velocity used by regulator
    ObjectId('velocity','default', 0x185), # Velocity computed from encoders
    ObjectId('error','default', 0x186), # Error from regulator
    ObjectId('error','sum', 0x187), # Sum of error from regulator
    ObjectId('pwm','default', 0x188), # PWM from regulator
    ObjectId('pwm','set', 0x189), # Direct PWM set (disable regulator)
    ObjectId('velocity','action', 0x18A), # Action velocity in regulator
    ObjectId('position','set', 0x18B), # Set position to motor module (clear encoder values)
    ObjectId('angle','set', 0x18C), # Set angle to motor module (clear encoder values)
    
    ObjectId('ir','top', 0x001),
    ObjectId('ir','bottom', 0x002),
    
    ObjectId('servo','top', 0x281),
    ObjectId('servo','bottom', 0x282),
    
    ObjectId('sharp','top', 0x291),
    ObjectId('sharp','bottom', 0x292),
    
    ObjectId('output','default', 0x381),
]