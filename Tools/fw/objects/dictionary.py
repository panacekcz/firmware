#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.

from fw.object import ConfigObject, ProcessObject

modules = []
pdo_id = []

sdo = [
    ConfigObject(0x0201, "encoder_config", "<3f", "top_diameter bot_diameter step"),
    ConfigObject(0x0202, "driver_config", "<6f", "kp_v ki_v kd_v kp_a ki_a kd_a"),
]

pdo = [
    ProcessObject("driver", "<ff", "forward angular"),
    ProcessObject("position", "<ff", "x y"),
    ProcessObject("angle", "<f", "angle"),
    ProcessObject("velocity", "<ff", "forward angular"),
    ProcessObject("encoders", "<ii", "top bottom"),
    ProcessObject("ir", "<B", "value"),
    ProcessObject("pwm", "<hh", "top bottom"),
    ProcessObject("error", "<ff", "forward angular"),
    
    ProcessObject("servo", "<BBBB", "a b c d"),
    ProcessObject("sharp", "<H", "value"),
    
    ProcessObject("output", "<I", "enable"),
]