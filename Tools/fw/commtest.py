#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import fw.comm as comm

class TimedTestCase(unittest.TestCase):
    def setUp(self):
        self.timeout = comm.Timeout(1)
    def tearDown(self):
        self.timeout.close()

class CanTestCase(unittest.TestCase):
    def setUp(self):
        self.timeout = comm.Timeout(1)
        self.can = comm.createCan(self.timeout)
    def tearDown(self):
        self.can.close()
        self.timeout.close()


class SerialTestCase(unittest.TestCase):
    def setUp(self):
        self.timeout = comm.Timeout(1)
        self.serial = comm.createPrimarySerial(self.timeout)
    def tearDown(self):
        self.serial.close()
        self.timeout.close()
    def writeCommand(self, *args):
        self.serial.writeCommand(*args)
    def expectCommand(self, *args):
        self.timeout.time = 1
        self.serial.expectCommand(self, *args)
    def expectCommandPattern(self, *args):
        self.timeout.time = 1
        self.serial.expectCommandPattern(self, *args)
    def expectNothing(self, *args):
        self.timeout.time = 0.1
        self.serial.expectNothing(self)
