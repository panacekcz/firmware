#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.

from fw.commtest import SerialTestCase

class SerialLoaderIhexTest(SerialTestCase):
    '''
    Test loader serial commands.
    Take a module with enabled serial (or USB) and connect it via serial (or USB).
    This test should pass only in loader mode.
    '''
    def testInvalidChar(self):
        #Do not ignore invalid characters
        self.writeCommand(':','X')
        self.expectCommand('x','HXC')
        #Do not ignore whitespace
        self.writeCommand(':',' 020000040800F2')
        self.expectCommand('x','HXC')
        #Do not ignore end of line
        self.writeCommand(':','020000040800F2 ')
        self.expectCommand('x','HXC')
        #Do not ignore invalid digits
        self.writeCommand(':','020000040gg800F2')
        self.expectCommand('x','HXC')
        #Check for invalid characters before check for length
        self.writeCommand(':','020000040g800F2')
        self.expectCommand('x','HXC')
        self.expectNothing()
    
    def testInvalidLength(self):
        #Empty lines
        self.writeCommand(':')
        self.expectCommand('x','HXL')
        #Odd length
        self.writeCommand(':','0')
        self.expectCommand('x','HXL')
        #Too short
        self.writeCommand(':','00')
        self.expectCommand('x','HXL')
        #Specified too long
        self.writeCommand(':','030000040800F2')
        self.expectCommand('x','HXL')
        #Specified too short
        self.writeCommand(':','020000040800F200')
        self.expectCommand('x','HXL')
        #Odd length
        self.writeCommand(':','020000040800F20')
        self.expectCommand('x','HXL')
        self.expectNothing()
    
    def testInvalidChecksum(self):
        #Corrupted checksum
        self.writeCommand(':','02000004080000')
        self.expectCommand('x','HXS')
        #Corrupted data
        self.writeCommand(':','02FF00040800F2')
        self.expectCommand('x','HXS')
        #Corrupted command
        self.writeCommand(':','020000FF0800F2')
        self.expectCommand('x','HXS')
        self.expectNothing()
    
    def testInvalidCommand(self):
        #With data
        self.writeCommand(':','020000F2080004')
        self.expectCommand('x','HXTf2')
        #Without data
        self.writeCommand(':','000000A55B')
        self.expectCommand('x','HXTa5')
        self.expectNothing()
        