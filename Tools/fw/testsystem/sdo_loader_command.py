#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.


import fw.upload as up
import fw.config as config

from fw.commtest import CanTestCase
class SdoLoaderCommandTest(CanTestCase):
    '''
    Test loader SDO commands.
    This test should pass only in loader mode.
    
    This test does not write any flash.
    '''
    
    node_id = config.getLoaderNodeId()
    
    def setUp(self):
        CanTestCase.setUp(self)
        self.client = up.CanopenUploaderClient(self.can, self.node_id)

    def testEndOperation(self):
        '''
        Test that the loader acceps an "end operation" command.
        Does not check any result, only that no error is returned.
        '''
        self.client.endOperation()

    def testDownload(self):
        '''
        Test that data can be downloaded from a address within FLASH.
        The data is not checked, but it is checked that the returned
        data have the correct length, for values 1 and 16.
        '''
        data = self.client.downloadData(0x08000000, 1)
        self.assertEqual(len(data), 1)
        data = self.client.downloadData(0x08000000, 16)
        self.assertEqual(len(data), 16)

    def testAddresses(self):
        '''
        Test the address SDO object.
         The address SDO object contains 3 addresses (uint32_t) and allows accessing
         them at once (sub-index = 0) or individually.
         
        Writes the whole objects and then each part individually.
        After each write, read the whole object and each part individually and check consistency. 
        '''
        self.client.setAddresses(0x00112233, 0x44556677, 0x8899aabb)
        self.expectAddresses(0x00112233, 0x44556677, 0x8899aabb)
        
        self.client.setCurrentAddress(0x12345678)
        self.expectAddresses(0x12345678, 0x44556677, 0x8899aabb)
        
        self.client.setLowAddress(0x456789ab)
        self.expectAddresses(0x12345678, 0x456789ab, 0x8899aabb)
        
        self.client.setHighAddress(0x89abcdef)
        self.expectAddresses(0x12345678, 0x456789ab, 0x89abcdef)
        
    def expectAddresses(self, current, low, high):
        self.assertEqual((current, low, high), self.client.getAddresses())
        self.assertEqual(current, self.client.getCurrentAddress())
        self.assertEqual(low, self.client.getLowAddress())
        self.assertEqual(high, self.client.getHighAddress())