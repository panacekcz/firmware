#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.

from fw.commtest import SerialTestCase

class SerialLoaderCommandTest(SerialTestCase):
    '''
    Test loader serial commands.
    Take a module with enabled serial (or USB) and connect it via serial (or USB).
    This test should pass only in loader mode.
    '''
    
    def testResetReason(self):
        self.writeCommand('l','s')
        self.expectCommandPattern('a', r'[0-9a-f]{4}')
        self.expectNothing()
    
    def testMessage(self):
        self.writeCommand('l','t')
        self.expectCommandPattern('a', r'LOA[0-9a-f]{2}')
        self.expectNothing()
    
    def testAddresses(self):
        self.writeCommand('l','c')
        self.expectCommandPattern('a', r'[0-9a-f]{8};[0-9a-f]{8}-[0-9a-f]{8}')
        self.expectNothing()

    def testSetAddresses(self):
        self.writeCommand('l','A')
        self.writeCommand('l','L')
        self.writeCommand('l','R')
        self.writeCommand('l','F')
        self.writeCommand('l','E')
        self.expectNothing()
