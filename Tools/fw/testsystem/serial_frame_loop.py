#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.

from fw.commtest import TimedTestCase 

import fw.comm as comm
import random
import struct
from binascii import b2a_hex

class SerialFrameLoopTest(TimedTestCase):
    '''
    Test that Slcan frames sent to one serial arrive at the other serial.
    This test does not work when there is other communication (heartbeat, etc).
    '''

    def setUp(self):
        TimedTestCase.setUp(self)
        self.ser0 = comm.createPrimarySerial(self.timeout)
        self.ser1 = comm.createSecondarySerial(self.timeout)

    def tearDown(self):
        self.ser0.close()
        self.ser1.close()
        TimedTestCase.tearDown(self)

    def testFrom0To1(self):
        self.ser0.writeCommand('o', 'uc')
        self.ser1.writeCommand('o', 'uc')
        
        self.ser0.writeCommand('t', '0000')
        self.ser1.expectCommand(self, 't', '0000')
        self.ser0.expectNothing(self)
        self.ser1.expectNothing(self)
    
    def testFrom1To0(self):
        self.ser0.writeCommand('o', 'uc')
        self.ser1.writeCommand('o', 'uc')
        
        self.ser1.writeCommand('t', '0000')
        self.ser0.expectCommand(self, 't', '0000')
        self.ser0.expectNothing(self)
        self.ser1.expectNothing(self)
    
    def testRandom(self):
        self.ser0.writeCommand('o', 'uc')
        self.ser1.writeCommand('o', 'uc')
        
        # Always use the same seed to avoid non-reproducible failures
        random.seed(12345)
        for _t in range(0, 1000):
            back = random.randint(0,1) == 0
            serr = self.ser0 if (back) else self.ser1
            serw = self.ser1 if (back) else self.ser0
            
            length = random.randint(0, 8)
            randdata = struct.pack('<Q',random.getrandbits(64))
            randdata = randdata[0:length]
            randtext = '000{0:x}{1}'.format(length, b2a_hex(format(randdata)))
            serw.writeCommand('t', randtext)
            serr.expectCommand(self, 't', randtext)
        
        self.ser0.expectNothing(self)
        self.ser1.expectNothing(self)