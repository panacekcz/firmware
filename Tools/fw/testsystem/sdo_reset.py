#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.

import fw.comm as comm
import fw.config as config
import fw.upload as upload
from fw.commtest import TimedTestCase

class SdoResetTest(TimedTestCase):
    '''This test passes only from application'''
    
    loader_reason = 0x0102
    app_reason = 0x0101
    
    def checkAndReset(self, node_id, expected_reason, reset_reason, print_message):
        with comm.createCan(self.timeout) as can:
            nmt = upload.CanopenNmtMaster(can)
            reason = nmt.getResetReason(node_id)
            if expected_reason is not None:
                self.assertEqual(reason, expected_reason)
            if print_message is not None:
                print(print_message)
            if reset_reason is not None:
                nmt.reset(node_id, reset_reason)
                can.expectDisconnect(self)
        

    def testReset(self):
        '''Test reset from both loader and app to both loader and app'''
        loader_node_id = config.getLoaderNodeId()
        app_node_id = config.getNodeId()
        
        self.checkAndReset(app_node_id, None, self.loader_reason, 'Initial reset into loader')
        self.checkAndReset(loader_node_id, self.loader_reason, self.loader_reason, 'Reset from loader into loader')
        self.checkAndReset(loader_node_id, self.loader_reason, self.app_reason, 'Reset from loader into app')
        self.checkAndReset(app_node_id, self.app_reason, self.app_reason, 'Reset from app into app')
        self.checkAndReset(app_node_id, self.app_reason, self.loader_reason, 'Reset from app into loader')
        self.checkAndReset(loader_node_id, self.loader_reason, None, None)
        pass
