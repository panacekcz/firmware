#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.

import os.path
import shutil

# TODO: This is not a good way to deal with configuration.
# However, it works for now.

configuration = {}
objects = {}
loaded = False

def load():
    global configuration
    global loaded
    global objects
    if not loaded:
        localname = os.path.join(os.path.dirname(__file__),"local.cfg")
        if not os.path.isfile(localname):
            defaultname = os.path.join(os.path.dirname(__file__),"default.cfg")
            shutil.copy(defaultname, localname)
        execfile(localname,configuration)
        
        if "objects" in configuration:
            for name in configuration["objects"]:
                localname = os.path.join(os.path.dirname(__file__), "objects", name + ".py")
                execfile(localname, objects)
        
        loaded = True
    return configuration

def getModules():
    global objects
    load()
    return objects["modules"]
def getSdos():
    global objects
    load()
    return objects["sdo"]
def getPdos():
    global objects
    load()
    return objects["pdo"]
def getPdoIds():
    global objects
    load()
    return objects["pdo_id"]
def getRobot():
    return load()["robot"]

def getNodeId():
    return load()["node_id"]

def getLoaderNodeId():
    return load()["loader_node_id"]

def getCan():
    return load()["can"]

def getPrimarySerial():
    return load()["primary_serial"]

def getSecondarySerial():
    return load()["secondary_serial"]

def getLoaderSerial():
    return load()["loader_serial"]

def getSerialRetryCount():
    return load()["serial_retry_count"]

def getSerialRetryInterval():
    return load()["serial_retry_interval"]

def getDisconnectOnReset():
    return load()["disconnect_on_reset"]

def getBuildConfiguration():
    return load()["build_configuration"]

def getCompiler():
    return load().get("compiler")

def getUploader():
    return load().get("uploader")


def getStrictComm():
    return load().get("strict_comm")

def getSerialVerbose():
    return load()["serial_verbose"]

def getSlcanInit():
    return load()["slcan_init"]


def getWorkspace():
    config = load()
    if "workspace" in config:
        return load()["workspace"]
    else:
        return os.path.join(os.path.dirname(__file__),'..','..')