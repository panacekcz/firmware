#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
import re

def decode(value, fmt):
    current = 32
    for part in fmt.split(" "):
        match = re.match("(\w+)?(\[(\d+)\])?", part)
        if match.group(3) is None:
            size = 1
        else:
            size = int(match.group(3))
        
        if match.group(1) is not None:
            fieldvalue = (value>>(current-size)) & ((1<<size)-1)
            print " {1}:\t{0}\t{0:#x}".format(fieldvalue, match.group(1))
        
        current -= size 
    if current != 0:
        raise Exception("Invalid format")
    
         
class SerialDiagnostics(object):

    def getValue(self):
        if self.timeout:
            return "Skipped"
        response = self.serial.readCommand('d')
        if response is None or response[0] != 'd':
            self.timeout = True
            return "Timeout"
        elif response[1] == "--------":
            return "Not supported"
        else:
            try:
                return int(response[1], 16)
            except ValueError:
                return "Invalid"   

    def getDiagnostics(self):
        self.serial.writeCommand('d')
        self.timeout = False
        self.sdo_errors = self.getValue()
        self.pdo_missed = self.getValue()
        self.can_missed_transmissions = self.getValue()
        self.can_missed_fast = self.getValue()
        
        self.can_mcr_reg = self.getValue()
        self.can_msr_reg = self.getValue()
        self.can_btr_reg = self.getValue()
        self.can_tsr_reg = self.getValue()
        self.can_esr_reg = self.getValue()
    
    def printDecoded(self, name, value, fmt):
        try:
            value = int(value)
            print name
            decode(value, fmt)
        except ValueError:
            print name, ":", value
    
    def output(self):
        print "SDO errors: {0}".format(self.sdo_errors)
        print "PDO frames not transmitted: {0}".format(self.pdo_missed)
        print "CAN frames not transmitted: {0}".format(self.can_missed_transmissions)
        print "CAN frames discarded: {0}".format(self.can_missed_fast)
        self.printDecoded("MCR", self.can_mcr_reg, "[15] DBF RESET [7] TTCM ABOM AWUM NART RFLM TXFP SLEEP INRQ")
        self.printDecoded("MSR", self.can_msr_reg, "[20] RX SAMP RXM TXM [3] SLAKI WKUI ERRI SLAK INAK")
        self.printDecoded("BTR", self.can_btr_reg, "SILM LBKM [4] SJW[2] [1] TS2[3] TS1[4] [6] BRP[10]")
        self.printDecoded("TSR", self.can_tsr_reg, "LOW2 LOW1 LOW0 TME2 TME1 TME0 CODE[2] ABRQ2 [3] TERR2 ALST2 TXOK2 ROCP2 ABRQ1 [3] TERR1 ALST1 TXOK1 ROCP1 ABRQ0 [3] TERR0 ALST0 TXOK0 ROCP0")
        self.printDecoded("ESR", self.can_esr_reg, "REC[8] TEC[8] [9] LEC[3] [1] BOFF EPVF EWGF")

    def __init__(self, serial):
        self.serial = serial