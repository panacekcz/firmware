#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.

from fw.commtest import CanTestCase 
import fw.config as config
import fw.upload as upload
from fw.protocol import sdoInt
import fw.protocol as protocol

class CommTest(CanTestCase):
    '''Test ExampleModule'''
    node_id = config.getNodeId()
    
    def setUp(self):
        CanTestCase.setUp(self)
        self.canopen = protocol.Canopen(self.can)
        self.sdo = protocol.CanopenSdo(self.canopen)
        self.nmt =  upload.CanopenNmtMaster(self.can)
    
    def doTestSum(self, operand_1, operand_2, result):
        # Write the operands
        self.sdo.writeObject(self.node_id, 0x0101, 0x00, sdoInt(operand_1))
        self.sdo.writeObject(self.node_id, 0x0102, 0x00, sdoInt(operand_2))
        # Go to operational state
        self.nmt.setOperational(self.node_id)
        # Receive PDOs 
        self.canopen.expectPdo(self, 0x001, sdoInt(result))
        # Go to pre-operational state
        self.nmt.setPreoperational(self.node_id)
    
    def testSum(self):
        self.doTestSum(0x00000000, 0x00000000, 0x00000000)
        self.doTestSum(0x5A5A5A5A, 0xA5A5A5A5, 0xFFFFFFFF)
        self.doTestSum(0xFFFFFFFF, 0x00000001, 0x00000000)
        self.doTestSum(0x342FBD1F, 0x34404F00, 0x68700c1f)
