#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.

from fw.commtest import TimedTestCase
import fw.config as config
from fw.protocol import sdoInt
import fw.comm as comm
import fw.protocol as protocol
from  fw.testmodule.comm import sdoTestData

class SdoBenchmarkTest(TimedTestCase):
    '''
    Test the timing of SDO commands
    '''
    
    node_id = config.getNodeId()
    
    serial = None
    can = None
    canopen = None
    
    def setUp(self):
        TimedTestCase.setUp(self)
        self.serial = comm.createPrimarySerial(self.timeout)
        self.can = comm.createCanComm(comm.Slcan(self.serial))
        self.canopen = protocol.Canopen(self.can)
        self.sdo = protocol.CanopenSdo(self.canopen)
    
    def tearDown(self):
        if self.can is not None:
            self.can.close()
        if self.serial is not None:
            self.serial.close()
        TimedTestCase.tearDown(self)
    
    def testSdoReal(self):
        '''
        Test 1000 reads of a SDO object.
        '''
        for _i in range(0, 1000):
            self.canopen.writeSdoShort(self.node_id, 0x40, 0x0101, 0x00, sdoInt(0))
            self.canopen.expectSdoShort(self, self.node_id, 0x4F, 0x0101, 0x00, sdoTestData(1, 0x0101, 0x00))
        pass
    
    def testSdoFake(self):
        '''
        Test sending and receiving 1000 commands with similar size as slcan frames.
        '''
        for _i in range(0, 1000):
            self.serial.writeCommand('b','aaaldddddddddddddddd')
            self.serial.expectCommand(self, 'b','aaaldddddddddddddddd')
        pass