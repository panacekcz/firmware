# Example feature project

## Create new feature in Eclipse

*File* > *New* > *C Project*
*Project type*: *Makefile project* > *Empty project*
*Toolchain*: *Cross ARM GCC*

### Project properties

Set the encoding to `UTF-8` and line delimiter to Unix.
Alternatively, you can copy the `.settings/org.eclipse.core.runtime.prefs`
and `.settings/org.eclipse.core.resources.prefs` from this project.

In *C/C++ Build* > *Behaviour*, uncheck *Build* and *Clean*.

### Feature files

Create any number of `.h` and `.c` files. The names of the header files must be unique
among the whole repository! 
In `feature.mk`, list all `.c` files and include directories.

You can use conditional compilation, use `#if EXAMPLE_SETTING`, not `#ifdef EXAMPLE_SETTING`.
The module using the feature will then define the settings in `moduleconf.h`.