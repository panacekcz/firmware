/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef	EXAMPLE_H_
#define EXAMPLE_H_

// Include header files

// This feature includes module.h, because it contains the declaration
// for ModuleConfigObject
#include "module.h"

// Settings

// This is an example setting, which is by default FALSE and modules can override
// it in their moduleconf.h file
#if defined(__DOXYGEN__) || !defined(EXAMPLE_ENABLE_LEDS)
#define EXAMPLE_ENABLE_LEDS FALSE
#endif

// Declare functions
// Use medial capitals notation
// The module can call the declared functions from moduleMain
// You should use a common prefix for all functions and global variables.

void exampleStartThread(void);
void exampleInit(void);

// Declare extern global variables
// Use underscore separated lowercase notation

// This feature defines 2 configuration objects. The module can enable them
// by adding a moduleConfigObjectHandler SDO handlers with pointers to these
// global variables as a parameter.
extern const ModuleConfigObject example_operand_1;
extern const ModuleConfigObject example_operand_2;


#endif /* EXAMPLE_H_ */
