/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

// Include headers
#include "example.h"

#if EXAMPLE_ENABLE_LEDS
#include "testshieldtest.h"
#endif

// Define global variables

// Use static for variables used only from one file
static uint32_t operand_1, operand_2;
static Mutex mutex;
static WORKING_AREA(example_wa, 128);

// Definition of SDO objects
// The IDs are assigned in the module.
const ModuleConfigObject example_operand_1 = {&operand_1, &mutex, (const uint8_t[]){4}, 1, NULL, 0};
const ModuleConfigObject example_operand_2 = {&operand_2, &mutex, (const uint8_t[]){4}, 1, NULL, 0};


// Define functions

void exampleInit(void){
	chMtxInit(&mutex);
	operand_1=0x55555555;
	operand_2=0xAAAAAAAA;
}

static msg_t exampleThread(void* arg){
	(void)arg;
	// This function periodically sums two integers and sends the result as a PDO
	for(;;){
		chThdSleepMilliseconds(1000);
		// The access to the configuration SDO objects should be protected with
		// a mutex
		chMtxLock(&mutex);
		uint32_t result = operand_1 + operand_2;
		chMtxUnlock();
		// To send a pdo, create a CommFrame object and fill all the fields.
		CommFrame pdo_frame;
		pdo_frame.data32[0] = result;
		pdo_frame.length = 4;
		pdo_frame.sid = 0x181;

		if(commNmtState() == COMM_NMT_OPERATIONAL){
			// Send the PDO. The second argument specifies that in case of a full buffer,
			// the function should not block and the frame should be thrown away.
			commPdoSend(&pdo_frame, TIME_IMMEDIATE);
		}

#if EXAMPLE_ENABLE_LEDS
		// This is a part that can be enabled by the module
		testShieldShow(result);
#endif

	}

	return 0;
}

// This function is called once in moduleMain and starts
// a new thread which does the work.
// For a feature this simple it may be an overkill to
// create a new thread, you can use callbacks, event loop
// or other patterns.

void exampleStartThread(void){
	chThdCreateStatic(example_wa, sizeof(example_wa), NORMALPRIO, exampleThread, NULL);
}
