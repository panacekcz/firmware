# Motor feature

## Configuration

In `moduleconf.h` in your module, configure the PWM drivers (for example see `Motors/default/moduleconf.h`).
One or both shields should be MotorShields.

In `moduleInit`, start the PWMs (`motorsStart()`).