/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "motors.h"
#include "hal.h"
#include "armboard.h"

#if SHIELD_TOP_ID == SHIELD_ID_MOTOR
/// Configuration for top PWMD5.
static const PWMConfig pwm_top_config = {
	MOTORS_PWM_TIMER_FREQ,
	MOTORS_PWM_PERIOD,
	NULL,
	{
		{PWM_OUTPUT_ACTIVE_HIGH, NULL},//TIM5_CH1 on PA0
		{PWM_OUTPUT_ACTIVE_HIGH, NULL},//TIM5_CH2 on PA1
		{PWM_OUTPUT_DISABLED, NULL},
		{PWM_OUTPUT_DISABLED, NULL}
	},
	0,
	0,
	0,
};
const Motor motor_top = {
	&PWMD5, 1, 0, // TIM5, CHN2, CHN1
	GPIOC, GPIOC_MOTOR_DIR2_TOP, // PC5
	GPIOC, GPIOC_MOTOR_DIR3_TOP, // PC4
};
#endif
#if SHIELD_BOTTOM_ID == SHIELD_ID_MOTOR
/// Configuration for bottom PWMD1.
static const PWMConfig pwm_bottom_config = {
	MOTORS_PWM_TIMER_FREQ,
	MOTORS_PWM_PERIOD,
	NULL,
	{
		{PWM_OUTPUT_DISABLED, NULL},
		{PWM_COMPLEMENTARY_OUTPUT_ACTIVE_HIGH, NULL}, //TIM1_CH2N on PB0
		{PWM_COMPLEMENTARY_OUTPUT_ACTIVE_HIGH, NULL}, //TIM1_CH3N on PB1
		{PWM_OUTPUT_DISABLED, NULL}
	},
	0,
	0,
	0,
};
const Motor motor_bottom = {
	&PWMD1, 1, 2, // TIM1, CHN2, CHN3
	GPIOC, GPIOC_MOTOR_DIR2_BOTTOM, // PC3
	GPIOC, GPIOC_MOTOR_DIR3_BOTTOM, // PC2
};
#endif

void motorSet(const Motor* motor, int16_t value){
	chSysLock();
	motorSetI(motor, value);
	chSysUnlock();
}
void motorFree(const Motor* motor){
	chSysLock();
	motorFreeI(motor);
	chSysUnlock();
}
void motorBrake(const Motor* motor){
	chSysLock();
	motorBrakeI(motor);
	chSysUnlock();
}

void motorSetI(const Motor* motor, int16_t value){
	if(value > 0)
		motorForwardI(motor, value);
	else if(value < 0)
		motorBackwardI(motor, -value);
	else
		motorFreeI(motor);
}
void motorForwardI(const Motor* motor, uint16_t value){
	if(value >= MOTORS_PWM_PERIOD)
		value = MOTORS_PWM_PERIOD-1; //TODO: check this

	// TODO: dir values?
	palSetPad((GPIO_TypeDef*)motor->dir2_gpio, motor->dir2_pin);
	palClearPad((GPIO_TypeDef*)motor->dir3_gpio, motor->dir3_pin);

	pwmEnableChannelI(motor->pwm, motor->pwm_ch2,  value);
	pwmEnableChannelI(motor->pwm, motor->pwm_ch3, value);
}
void motorBackwardI(const Motor* motor, uint16_t value){
	if(value >= MOTORS_PWM_PERIOD)
		value = MOTORS_PWM_PERIOD-1; //TODO: check this

	// TODO: dir values?
	palClearPad((GPIO_TypeDef*)motor->dir2_gpio, motor->dir2_pin);
	palSetPad((GPIO_TypeDef*)motor->dir3_gpio, motor->dir3_pin);

	pwmEnableChannelI(motor->pwm, motor->pwm_ch2, value);
	pwmEnableChannelI(motor->pwm, motor->pwm_ch3, value);
}
void motorFreeI(const Motor* motor){
	// TODO: dir values not important?
	palClearPad((GPIO_TypeDef*)motor->dir2_gpio, motor->dir2_pin);
	palClearPad((GPIO_TypeDef*)motor->dir3_gpio, motor->dir3_pin);
	// Disable pwm
	pwmDisableChannelI(motor->pwm, motor->pwm_ch2);
	pwmDisableChannelI(motor->pwm, motor->pwm_ch3);
}
void motorBrakeI(const Motor* motor){
	// TODO: dir values low?
	palClearPad((GPIO_TypeDef*)motor->dir2_gpio, motor->dir2_pin);
	palClearPad((GPIO_TypeDef*)motor->dir3_gpio, motor->dir3_pin);
	// Enable pwm
	pwmEnableChannelI(motor->pwm, motor->pwm_ch2, MOTORS_PWM_PERIOD-1);//TODO: check this
	pwmEnableChannelI(motor->pwm, motor->pwm_ch3, MOTORS_PWM_PERIOD-1);
}


void motorsStart(void){
#if SHIELD_TOP_ID == SHIELD_ID_MOTOR
	pwmStart(motor_top.pwm, &pwm_top_config);
#endif
#if SHIELD_BOTTOM_ID == SHIELD_ID_MOTOR
	pwmStart(motor_bottom.pwm, &pwm_bottom_config);
#endif
}
