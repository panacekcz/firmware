/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort, Ondřej Holešovský

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "motordriver.h"
#include "motors.h"
#include "encoders.h"
#include "ch.h"
#include <math.h>
#include "armboard.h"

#if SHIELD_TOP_ID == SHIELD_ID_MOTOR && SHIELD_BOTTOM_ID == SHIELD_ID_MOTOR

//TODO: check math "//{0.0f,0.0f,0.0f,0.0f,0.0f,0.0f};//"
DriverConfig regulation_config = {5.f,0.5f,0.0f,10.f,0.1f,0.0f};
DriverTarget regulation_target;
bool_t regulation_enabled;

ModuleConfigObject driver_config_object = {&regulation_config, NULL, (const uint8_t[]){4,8,12,16,20,24}, 6, NULL, 0};

void driverEncodersUpdatedDemo(void){
	chSysLockFromIsr();
	motorSetI(&motor_top, encoder_top.value/10);
	motorSetI(&motor_bottom, encoder_bottom.value/10);
	chSysUnlockFromIsr();
}

void driverTargetHandler(CommHandlerEvent evt){
	(void)evt;

	chSysLock();
	regulation_target.forward = comm_frame.dataF[0];
	regulation_target.angle = comm_frame.dataF[1];
	regulation_enabled = true;
	chSysUnlock();
}

void driverPwmHandler(CommHandlerEvent evt){
	(void)evt;

	chSysLock();
	regulation_enabled = false;
	chSysUnlock();
	motorSet(&motor_top, comm_frame.dataF[0]);
	motorSet(&motor_bottom, comm_frame.dataF[1]);
}

typedef struct{
	float a, v;
} RegulationValue;

RegulationValue error_sum;
RegulationValue error_last;

static CommFrame driver_frame;

void driverTransmitValue(uint16_t id, const RegulationValue* v){

	driver_frame.length = 8;
	driver_frame.dataF[0] = v->v;
	driver_frame.dataF[1] = v->a;
	driver_frame.sid = 0x180 + id;

	chSysLockFromIsr();
	if(commNmtStateI() == COMM_NMT_OPERATIONAL)
		commPdoSendI(&driver_frame);
	chSysUnlockFromIsr();
}

void driverTransmitPwm(uint16_t id, int16_t top, int16_t bot){
	driver_frame.length = 4;
	driver_frame.data16[0] = top;
	driver_frame.data16[1] = bot;
	driver_frame.sid = 0x180 + id;

	chSysLockFromIsr();
	if(commNmtStateI() == COMM_NMT_OPERATIONAL)
		commPdoSendI(&driver_frame);
	chSysUnlockFromIsr();
}
static RegulationValue error;
static RegulationValue error_diff;
static RegulationValue action;
static RegulationValue value;

static DriverTarget driver_current_target;
static DriverConfig driver_current_config;
static EncoderConfig driver_encoder_config;

static int16_t driver_enc_top, driver_enc_bottom;

void driverEncodersUpdated(void){
	static char saturated = 0;

	chSysLockFromIsr();
	driver_enc_top = encoder_top.diff;
	driver_enc_bottom = encoder_bottom.diff; //TODO: configurable sign (prev -)

	driver_current_target = regulation_target;
	driver_current_config = regulation_config;
	driver_encoder_config = enc_config;

	chSysUnlockFromIsr();

	value.a = driver_enc_top / driver_encoder_config.top_diameter + driver_enc_bottom / driver_encoder_config.bottom_diameter;
	value.v = (driver_enc_top*driver_encoder_config.stepTop + driver_enc_bottom*driver_encoder_config.stepBottom) / 2;

	error.a = -value.a + driver_current_target.angle; //TODO: check math "signs inverted"
	error.v = -value.v + driver_current_target.forward; //TODO: check math "signs inverted"

	driverTransmitValue(DRIVER_ERROR_PDO , &error);

	if(!saturated){
		error_sum.a += error.a;
		error_sum.v += error.v;
	}

	if(driver_current_target.forward < 0.001f && driver_current_target.forward > -0.001f) //TODO: extract constants
		error_sum.v = 0.0f;
	if(driver_current_target.angle < 0.0001f && driver_current_target.angle > -0.0001f) //TODO: extract constants
		error_sum.a = 0.0f;

	driverTransmitValue(DRIVER_ERROR_SUM_PDO, &error_sum);

	error_diff.a = error.a - error_last.a;
	error_diff.v = error.v - error_last.v;

	error_last.a = error.a;
	error_last.v = error.v;

	//TODO: check math "+value.a"
	action.a = driver_current_config.kp_a * error.a + driver_current_config.ki_a * error_sum.a + driver_current_config.kd_a * error_diff.a;
	//TODO: check math "+value.v"
	action.v = driver_current_config.kp_v * error.v + driver_current_config.ki_v * error_sum.v + driver_current_config.kd_v * error_diff.v;
	float pw_top = -(action.v - action.a / 2 * driver_encoder_config.top_diameter);
	float pw_bottom = -(action.v + action.a / 2 * driver_encoder_config.bottom_diameter);

	driverTransmitValue(DRIVER_ACTION_VELOCITY_PDO, &action);
	saturated = 0;
	if(pw_top>MOTORS_PWM_PERIOD){
		pw_top =MOTORS_PWM_PERIOD;
		saturated = 1;
	}
	if(pw_top<-MOTORS_PWM_PERIOD){
		pw_top =-MOTORS_PWM_PERIOD;
		saturated = 1;
	}
	if(pw_bottom>MOTORS_PWM_PERIOD){
		pw_bottom =MOTORS_PWM_PERIOD;
		saturated = 1;
	}
	if(pw_bottom<-MOTORS_PWM_PERIOD){
		pw_bottom =-MOTORS_PWM_PERIOD;
		saturated = 1;
	}

	driverTransmitPwm(DRIVER_PWM_PDO, pw_top, pw_bottom);

	chSysLockFromIsr();
	if(regulation_enabled){
		motorSetI(&motor_top, pw_top);
		motorSetI(&motor_bottom, pw_bottom);
	}
	chSysUnlockFromIsr();
}
#endif




