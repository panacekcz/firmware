/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef	MOTORDRIVER_H_
#define MOTORDRIVER_H_

#include "module.h"

typedef struct{
	float kp_v, ki_v, kd_v;
	float kp_a, ki_a, kd_a;
}DriverConfig;
typedef struct{
	float forward, angle;
}DriverTarget;

extern ModuleConfigObject driver_config_object;

void driverEncodersUpdatedDemo(void);
void driverEncodersUpdated(void);

void driverTargetHandler(CommHandlerEvent event);
void driverPwmHandler(CommHandlerEvent event);

#define DRIVER_PWM_PDO 0x188
#define DRIVER_ERROR_PDO 0x186
#define DRIVER_ERROR_SUM_PDO 0x187
#define DRIVER_ACTION_VELOCITY_PDO 0x18A

#endif
