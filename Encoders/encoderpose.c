/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort, Ondřej Holešovský

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "encoders.h"
#include "communication.h"
#include <math.h>

#if SHIELD_TOP_ID == SHIELD_ID_MOTOR && SHIELD_BOTTOM_ID == SHIELD_ID_MOTOR

typedef struct{
	float x, y;
	float angle;
	float forward_vel;
	float angle_vel;
} Pose;

float angle0;

static int32_t last_top;
static int32_t last_bottom;

Pose pose = {0, 0, 0, 0, 0}; //TODO: synchronize
EncoderConfig enc_config = {953.7330f, 930.4909f, -0.2070f, 0.2152f}; //TODO: add step bottom and step top

const ModuleConfigObject encoder_config = {&enc_config, NULL, (const uint8_t[]){4,8,12}, 3, NULL, 0};

void encoderPoseUpdate(void){

	chSysLock();
	EncoderConfig current_config = enc_config;
	int32_t current_top = +encoder_top.value; //TODO: check math "sign changed (prev +)"
	int32_t current_bottom = encoder_bottom.value; //TODO: check math "configurable sign (prev -)"
	chSysUnlock();

	//TODO: check math "prev: angle = angle0 + ( ... and angle_vel = + ((..."
	pose.angle = angle0 + (current_top / current_config.top_diameter + current_bottom / current_config.bottom_diameter);
	pose.angle_vel = +((current_top - last_top) / current_config.top_diameter + (current_bottom - last_bottom) / current_config.bottom_diameter);

	float distance = (current_top - last_top)*current_config.stepTop + (current_bottom - last_bottom)*current_config.stepBottom;
	distance /= 2;
	//TODO: check math "distance *= current_config.step;"

	pose.forward_vel = distance;
	pose.x += distance * cos(pose.angle);
	pose.y += distance * sin(pose.angle); //TODO: check math "prev + distance"

	last_top = current_top;
	last_bottom = current_bottom;
}
void encoderPoseTransmit(void){
	CommFrame cf;
	cf.length = 8;
	cf.dataF[0] = pose.x;
	cf.dataF[1] = pose.y;
	cf.sid = 0x301;
	commPdoSend(&cf, TIME_IMMEDIATE);

	cf.length = 4;
	cf.dataF[0] = pose.angle;
	cf.sid = 0x302;
	commPdoSend(&cf, TIME_IMMEDIATE);

	cf.length = 8;
	cf.dataF[0] = pose.forward_vel;
	cf.dataF[1] = pose.angle_vel;
	cf.sid = 0x305;
	commPdoSend(&cf, TIME_IMMEDIATE);
}

void encoderPositionHandler(CommHandlerEvent event){
	(void)event;
	chSysLock();
	encodersClearI();
	angle0 = pose.angle;
	pose.x = comm_frame.dataF[0];
	pose.y = comm_frame.dataF[1];
	chSysUnlock();
}
void encoderAngleHandler(CommHandlerEvent event){
	(void)event;
	chSysLock();
	encodersClearI();
	angle0 = comm_frame.dataF[0];
	chSysUnlock();
}
#endif
