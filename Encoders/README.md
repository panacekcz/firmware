# Encoder feature

## Configuration

In `moduleconf.h` in your module, configure the GPT and QEI drivers (for example see `Encoders/default/moduleconf.h`).
One or both shields should be MotorShields.

In `moduleInit`, initialize the QEI driver (`qeiInit()`) and start the timers (`encodersStart()`).