/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef	ENCODERS_H_
#define ENCODERS_H_

#include "module.h"
#include "ch.h"

#if !defined(ENCODER_TIMER_FREQ)
/// Frequency of the timer controlling encoder updates
#define ENCODER_TIMER_FREQ 10000 //TODO: usable default
#endif
#if !defined(ENCODER_TIMER_PERIOD)
/// Period the timer controlling encoder updates, in ticks specified by the timer frequency.
#define ENCODER_TIMER_PERIOD 100 //TODO: usable default
#endif


/// Encoder values.
typedef struct {
	/// Total value of the encoder.
	int32_t value;
	/// Last value read from the counter register.
	uint16_t counter;
	/// Difference between the last two updates.
	int16_t diff;
}Encoder;

#if SHIELD_TOP_ID == SHIELD_ID_MOTOR
/// Encoder on the top shield.
extern Encoder encoder_top;
#endif

#if SHIELD_BOTTOM_ID == SHIELD_ID_MOTOR
/// Encoder on the bottom shield.
extern Encoder encoder_bottom;
#endif

#if SHIELD_TOP_ID == SHIELD_ID_MOTOR && SHIELD_BOTTOM_ID == SHIELD_ID_MOTOR
extern const ModuleConfigObject encoder_config;
#endif

#if !defined(ENCODER_CALLBACK) || defined(__DOXYGEN__)
/// Callback called after every update of encoder values.
#define ENCODER_CALLBACK() {}
#endif

typedef struct{
	float top_diameter;
	float bottom_diameter;
	float stepTop;
	float stepBottom;
} EncoderConfig;
extern EncoderConfig enc_config;

/// Enable encoder control.
void encodersStart(void);
void encodersTransmit(void);
void encoderPoseTransmit(void);
void encoderPoseUpdate(void);

void encodersClearI(void);

void encoderPositionHandler(CommHandlerEvent event);
void encoderAngleHandler(CommHandlerEvent event);

#endif
