/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "encoders.h"
#include "hal.h"
#include "qei.h"
#include "armboard.h"

#if !HAL_USE_QEI
#error Quadrature encoder interface not enabled
#endif
#if !HAL_USE_GPT
#error General purpose timers not enabled
#endif

/// Timer callback updating the encoder values.
static void encodersUpdate(GPTDriver *gptp);

/// Configuration of the timer controlling encoder updates.
static GPTConfig gpt_config = {
	ENCODER_TIMER_FREQ, encodersUpdate, 0
};

#if SHIELD_TOP_ID == SHIELD_ID_MOTOR
/// QEI driver configuration for the top encoder.
static QEIConfig qei_top_config = {
	QEI_MODE_QUADRATURE, QEI_BOTH_EDGES, QEI_DIRINV_FALSE
};
Encoder encoder_top;
#endif

#if SHIELD_BOTTOM_ID == SHIELD_ID_MOTOR
/// QEI driver configuration for the bottom encoder.
static QEIConfig qei_bottom_config = {
	QEI_MODE_QUADRATURE, QEI_BOTH_EDGES, QEI_DIRINV_FALSE
};
Encoder encoder_bottom;
#endif

/// Update the specified encoder values.
static void encoderUpdate(QEIDriver* driver, Encoder* encoder){
	uint16_t new_count = qeiGetCount(driver);

	// Subtract two unsigned 16-bit numbers.
	encoder->diff = (int16_t)(uint16_t)(new_count - encoder->counter);
	encoder->value += encoder->diff;
	encoder->counter = new_count;
}

static void encodersUpdate(GPTDriver *gptp){
	(void)gptp;

#if SHIELD_TOP_ID == SHIELD_ID_MOTOR
	encoderUpdate(&QEID3, &encoder_top);
#endif
#if SHIELD_BOTTOM_ID == SHIELD_ID_MOTOR
	encoderUpdate(&QEID2, &encoder_bottom);
#endif

	// Call the user specified callback.
	ENCODER_CALLBACK();
}

void encodersStart(void){
	gptStart(&GPTD6, &gpt_config);

#if SHIELD_TOP_ID == SHIELD_ID_MOTOR
	qeiStart(&QEID3, &qei_top_config);
	qeiEnable(&QEID3);
#endif
#if SHIELD_BOTTOM_ID == SHIELD_ID_MOTOR
	qeiStart(&QEID2, &qei_bottom_config);
	qeiEnable(&QEID2);
#endif


	gptStartContinuous(&GPTD6, ENCODER_TIMER_PERIOD);
}

static void encoderClearI(QEIDriver* driver, Encoder* encoder){
	uint16_t new_count = qeiGetCount(driver);
	encoder->diff = 0;
	encoder->value = 0;
	encoder->counter = new_count;
}

void encodersClearI(void){
#if SHIELD_TOP_ID == SHIELD_ID_MOTOR
	encoderClearI(&QEID3, &encoder_top);
#endif
#if SHIELD_BOTTOM_ID == SHIELD_ID_MOTOR
	encoderClearI(&QEID2, &encoder_bottom);
#endif
}

void encodersTransmit(void){
	CommFrame frame;

	frame.length = 8;
	frame.sid = 0x303;
	chSysLock();
#if SHIELD_TOP_ID == SHIELD_ID_MOTOR
	frame.data32[0] = encoder_top.value;
#else
	frame.data32[0] = 0;
#endif
#if SHIELD_BOTTOM_ID == SHIELD_ID_MOTOR
	frame.data32[1] = encoder_bottom.value;
#else
	frame.data32[1] = 0;
#endif
	chSysUnlock();
	commPdoSend(&frame, TIME_IMMEDIATE);
}

