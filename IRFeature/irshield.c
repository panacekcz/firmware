/*
	Robot Firmware - Copyright (C) 2014 Ondřej Holešovský

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "irshield.h" 

#if SHIELD_TOP_ID == SHIELD_ID_IR
const struct IRSens topIRSens[]=
{
	{GPIOC,J7r_TOP_C12,GPIOD,J7a_TOP_D2,GPIOA,J7b_TOP_A2},
	{GPIOC,J6r_TOP_C11,GPIOC,J6a_TOP_C10,GPIOA,J6b_TOP_A0},
	{GPIOB,J5r_TOP_B10,GPIOC,J5a_TOP_C1,GPIOC,J5b_TOP_C4},
	{GPIOB,J3r_TOP_B13,GPIOC,J3a_TOP_C5,GPIOA,J3b_TOP_A7},
	{GPIOB,J4r_TOP_B11,GPIOB,J4a_TOP_B12,GPIOA,J4b_TOP_A6},
};
#endif

#if SHIELD_BOTTOM_ID == SHIELD_ID_IR
const struct IRSens bottomIRSens[]=
{
	{GPIOB,J7r_BOTTOM_B15,GPIOC,J7a_BOTTOM_C6,GPIOA,J7b_BOTTOM_A3},
	{GPIOB,J6r_BOTTOM_B14,GPIOA,J6a_BOTTOM_A5,GPIOB,J6b_BOTTOM_B1},
	{GPIOB,J5r_BOTTOM_B6,GPIOA,J5a_BOTTOM_A4,GPIOC,J5b_BOTTOM_C2},
	{GPIOB,J3r_BOTTOM_B4,GPIOC,J3a_BOTTOM_C3,GPIOB,J3b_BOTTOM_B3},
	{GPIOB,J4r_BOTTOM_B7,GPIOB,J4a_BOTTOM_B5,GPIOA,J4b_BOTTOM_A15},
};
#endif
