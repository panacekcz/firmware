/*
	Robot Firmware - Copyright (C) 2014 Ondřej Holešovský

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "irsens.h"
#include "armboard.h"
#include "irshield.h"

static Mutex config_mutex;

#if (SHIELD_TOP_ID == SHIELD_ID_IR)
static WORKING_AREA(irsens_wa1, 128);
static struct IRPack topPack;
static struct IRConfig topConf;
static struct IRConfigCAN topConfCan;
const ModuleConfigObject irsens_top = {&topConfCan, &config_mutex, (const uint8_t[]){2,3,4,5}, 4, &topPack.eventSource, 1};

#if IRSHIELD_TOP_SHARP
#define IRSENS_TOP_ALL 0b00011011
#else
#define IRSENS_TOP_ALL 0b00011111
#endif

#endif

#if (SHIELD_BOTTOM_ID == SHIELD_ID_IR)
static WORKING_AREA(irsens_wa2, 128);
static struct IRPack bottomPack;
static struct IRConfig bottomConf;
static struct IRConfigCAN bottomConfCan;
const ModuleConfigObject irsens_bottom = {&bottomConfCan, &config_mutex, (const uint8_t[]){2,3,4,5}, 4, &bottomPack.eventSource, 1};

#if IRSHIELD_BOTTOM_SHARP
#define IRSENS_BOTTOM_ALL 0b00011011
#else
#define IRSENS_BOTTOM_ALL 0b00011111
#endif

#endif

// Definition of SDO objects
// The IDs are assigned in the module.


static void extractConfig(struct IRConfigCAN *msc, struct IRConfig *sc){
	uint8_t code;
	chMtxLock(&config_mutex);
	code = msc->code;
	sc->cid = msc->cid;
	sc->bitEnable=msc->bitEnable;
	sc->opt = msc->opt;
	chMtxUnlock();
	for(int i = 0;i<4;i++){
		sc->code[i] = (code&(3<<i*2))>>i*2;
	}
}

static void updateConfig(void){
#if (SHIELD_TOP_ID == SHIELD_ID_IR)
	extractConfig(&topConfCan,&topConf);
#endif
#if (SHIELD_BOTTOM_ID == SHIELD_ID_IR)
	extractConfig(&bottomConfCan,&bottomConf);
#endif
}

void irsensInit(void){
	// irsensInit is called before communication is initialized
	chMtxInit(&config_mutex);

#if (SHIELD_TOP_ID == SHIELD_ID_IR)
	topConfCan.cid=1;
	topConfCan.code=0;
	topConfCan.bitEnable=IRSENS_TOP_ALL;
	topConfCan.opt=0;
#endif


#if (SHIELD_BOTTOM_ID == SHIELD_ID_IR)
	bottomConfCan.cid=2;
	bottomConfCan.code=5;
	bottomConfCan.bitEnable=IRSENS_BOTTOM_ALL;
	bottomConfCan.opt=0;
#endif

	updateConfig();
}
static int checkOn(const struct IRSens *sens, uint8_t code, uint8_t expect){//default expect 0
	palSetPad(sens->aport, sens->apin);
	palSetPad(sens->bport, sens->bpin);
	chThdSleepMicroseconds(702);
	if(palReadPad(sens->rport, sens->rpin) != expect){
		return 0;
	}
	chThdSleepMicroseconds(151*(code%4)+3);
	if(palReadPad(sens->rport, sens->rpin) != expect){
		return 0;
	}
	chThdSleepMicroseconds(162);
	if(palReadPad(sens->rport, sens->rpin) != expect){
		return 0;
	}
	palClearPad(sens->aport, sens->apin);
	palClearPad(sens->bport, sens->bpin);
	return 1;
}

static int checkOff(const struct IRSens *sens, uint8_t code, uint8_t expect){ //default expect 1
	chThdSleepMicroseconds(459);
	if(palReadPad(sens->rport, sens->rpin) != expect){
		return 0;
	}
	chThdSleepMicroseconds(297);
	chThdSleepMicroseconds(53*(code%4)+3);
	if(palReadPad(sens->rport, sens->rpin) != expect){
		return 0;
	}
	chThdSleepMicroseconds(162);
	if(palReadPad(sens->rport, sens->rpin) != expect){
		return 0;
	}
	return 1;
}

#define EXPECT_0(sei) (!!(pack->invert&(1<<sei)))
#define EXPECT_1(sei) (!(pack->invert&(1<<sei)))

static void readSensors(struct IRPack* pack){
	
	pack->status=0;
	for(int sei=0;sei<pack->count;sei++){
		chThdSleepMilliseconds(9);
		if(!(pack->config->bitEnable&pack->mask&(1<<sei)))
			continue;
		if(!checkOn(&pack->sensors[sei],pack->config->code[0],EXPECT_0(sei)))
			continue;
		if(!checkOff(&pack->sensors[sei],pack->config->code[1],EXPECT_1(sei)))
			continue;
		
		if(!checkOn(&pack->sensors[sei],pack->config->code[2],EXPECT_0(sei)))
			continue;
		if(!checkOff(&pack->sensors[sei],pack->config->code[3],EXPECT_1(sei)))
			continue;
		pack->status|=(1<<sei);
		
	}
}

static msg_t irsensThread(void* arg){
	struct IRPack *pack;
	pack=(struct IRPack*)arg;
	
	EventListener listener;

	chEvtRegister(&pack->eventSource, &listener, 0);

	for(;;){
		readSensors(pack);
		
		CommFrame pdo_frame;
		pdo_frame.data8[0] = pack->status;
		pdo_frame.length = 1;
		pdo_frame.sid = 0x180+pack->config->cid;

		if(commNmtState() == COMM_NMT_OPERATIONAL){
			commPdoSend(&pdo_frame, TIME_IMMEDIATE);
		}

		if(chEvtGetAndClearFlags(&listener)){
			extractConfig(pack->canConfig, pack->config);
		}

	}

	return 0;
}

void irsensStartThread(void){

#if (SHIELD_TOP_ID == SHIELD_ID_IR)
	topPack.config=&topConf;
	topPack.sensors=topIRSens;
	topPack.count=IR_SENSORS;
	topPack.canConfig = &topConfCan;
	topPack.mask = IRSENS_TOP_ALL;
	topPack.invert = IRSENS_TOP_INVERT;

	chEvtInit(&topPack.eventSource);

	chThdCreateStatic(irsens_wa1, sizeof(irsens_wa1), NORMALPRIO, irsensThread, &topPack);
#endif
	
#if (SHIELD_BOTTOM_ID == SHIELD_ID_IR)

	bottomPack.config=&bottomConf;
	bottomPack.sensors=bottomIRSens;
	bottomPack.count=IR_SENSORS;
	bottomPack.canConfig = &bottomConfCan;
	bottomPack.mask = IRSENS_BOTTOM_ALL;
	bottomPack.invert = IRSENS_BOTTOM_INVERT;
	chEvtInit(&bottomPack.eventSource);
	chThdCreateStatic(irsens_wa2, sizeof(irsens_wa2), NORMALPRIO, irsensThread, &bottomPack);
#endif
}
