/*
	Robot Firmware - Copyright (C) 2014 Ondřej Holešovský

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef	IRSENS_H_
#define IRSENS_H_

#include "module.h"
#include "irshield.h" 

struct __attribute__ ((packed)) IRConfigCAN{
	uint16_t cid;
	uint8_t code;
	uint8_t bitEnable;
	uint8_t opt;
};

struct IRConfig{
	uint16_t cid;
	uint8_t code[4];
	uint8_t bitEnable;
	uint8_t opt;
};

struct IRPack{
	uint8_t status;
	uint8_t mask;
	uint8_t invert;
	int count;
	struct IRConfig *config;
	const struct IRSens *sensors;
	EventSource eventSource;
	struct IRConfigCAN* canConfig;
};

void irsensStartThread(void);
void irsensInit(void);

#ifndef IRSENS_TOP_INVERT
#define IRSENS_TOP_INVERT 0
#endif
#ifndef IRSENS_BOTTOM_INVERT
#define IRSENS_BOTTOM_INVERT 0
#endif

#if (SHIELD_TOP_ID == SHIELD_ID_IR)
extern const ModuleConfigObject irsens_top;
#endif
#if (SHIELD_BOTTOM_ID == SHIELD_ID_IR)
extern const ModuleConfigObject irsens_bottom;
#endif

#endif /* IRSENS_H_ */
