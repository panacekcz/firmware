/*
	Robot Firmware - Copyright (C) 2014 Ondřej Holešovský

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef	IRSHIELD_H_
#define IRSHIELD_H_

#include "armboard.h"
#include "module.h"

struct IRSens{
	ioportid_t rport;
	uint8_t rpin;

	ioportid_t aport;
	uint8_t apin;
	
	ioportid_t bport;
	uint8_t bpin;
};

/* Number of sensors on one shield. */
#define IR_SENSORS 5

/* ========== TOP SHIELD ========== */
#if SHIELD_TOP_ID == SHIELD_ID_IR

#define J7r_TOP_C12 12
#define J7a_TOP_D2 2
#define J7b_TOP_A2 2

#define J6r_TOP_C11 11
#define J6a_TOP_C10 10
#define J6b_TOP_A0 0

#define J5r_TOP_B10 10
#define J5a_TOP_C1 1
#define J5b_TOP_C4 4

#define J3r_TOP_B13 13
#define J3a_TOP_C5 5
#define J3b_TOP_A7 7

#define J4r_TOP_B11 11
#define J4a_TOP_B12 12
#define J4b_TOP_A6 6

#if IRSHIELD_TOP_SHARP
#define VAL_TOP_GPIOACR  0x0000000033000303ull
#define VAL_TOP_GPIOAODR 0b0000000000000000
#define VAL_TOP_GPIOBCR  0x00B3BB0000000000ull
#define VAL_TOP_GPIOBODR 0b0010110000000000
#define VAL_TOP_GPIOCCR  0x000BB30000300030ull//C4 analog in
#define VAL_TOP_GPIOCODR 0b0001100000000000
#define VAL_TOP_GPIODCR  0x0000000000000300ull
#define VAL_TOP_GPIODODR 0b0000000000000000
#else
#define VAL_TOP_GPIOACR  0x0000000033000303ull
#define VAL_TOP_GPIOAODR 0b0000000000000000
#define VAL_TOP_GPIOBCR  0x00B3BB0000000000ull
#define VAL_TOP_GPIOBODR 0b0010110000000000
#define VAL_TOP_GPIOCCR  0x000BB30000330030ull
#define VAL_TOP_GPIOCODR 0b0001100000000000
#define VAL_TOP_GPIODCR  0x0000000000000300ull
#define VAL_TOP_GPIODODR 0b0000000000000000
#endif

extern const struct IRSens topIRSens[];
#endif
/* ========== ======== ========== */

/* ========== BOTTOM SHIELD ========== */
#if SHIELD_BOTTOM_ID == SHIELD_ID_IR

#define J7r_BOTTOM_B15 15
#define J7a_BOTTOM_C6 6
#define J7b_BOTTOM_A3 3

#define J6r_BOTTOM_B14 14
#define J6a_BOTTOM_A5 5
#define J6b_BOTTOM_B1 1

#define J5r_BOTTOM_B6 6
#define J5a_BOTTOM_A4 4
#define J5b_BOTTOM_C2 2

#define J3r_BOTTOM_B4 4
#define J3a_BOTTOM_C3 3
#define J3b_BOTTOM_B3 3

#define J4r_BOTTOM_B7 7
#define J4a_BOTTOM_B5 5
#define J4b_BOTTOM_A15 15

#if IRSHIELD_BOTTOM_SHARP
#define VAL_BOTTOM_GPIOACR  0x3000000000333000ull
#define VAL_BOTTOM_GPIOAODR 0b0000000000000000
#define VAL_BOTTOM_GPIOBCR  0xBB000000BB3B3030ull
#define VAL_BOTTOM_GPIOBODR 0b1100000011010000
#define VAL_BOTTOM_GPIOCCR  0x0000000003003000ull//C2 analog in
#define VAL_BOTTOM_GPIOCODR 0b0000000000000000
#define VAL_BOTTOM_GPIODCR  0x0000000000000000ull
#define VAL_BOTTOM_GPIODODR 0b0000000000000000
#else
#define VAL_BOTTOM_GPIOACR  0x3000000000333000ull
#define VAL_BOTTOM_GPIOAODR 0b0000000000000000
#define VAL_BOTTOM_GPIOBCR  0xBB000000BB3B3030ull
#define VAL_BOTTOM_GPIOBODR 0b1100000011010000
#define VAL_BOTTOM_GPIOCCR  0x0000000003003300ull
#define VAL_BOTTOM_GPIOCODR 0b0000000000000000
#define VAL_BOTTOM_GPIODCR  0x0000000000000000ull
#define VAL_BOTTOM_GPIODODR 0b0000000000000000
#endif

extern const struct IRSens bottomIRSens[];
#endif
/* ========== ======== ========== */



#endif /* IRSHIELD_H_ */
