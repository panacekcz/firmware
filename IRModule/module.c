/*
	Robot Firmware - Copyright (C) 2014 Ondřej Holešovský

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "module.h"
#include "irsens.h"
#include "armboard.h"
#include "servo.h"
#include "sharp.h"

void moduleInit(void){
	irsensInit();
	servoStart();
	sharpStart();
}
void moduleMain(void){
	irsensStartThread();
	for(;;){
		chThdSleepMilliseconds(100);
		if(commNmtState() == COMM_NMT_OPERATIONAL)
			sharpTransmit();
	}
}

bool_t commNmtStateChanging(uint16_t o, uint16_t n){
	(void)o;
	(void)n;

	return TRUE;
}
